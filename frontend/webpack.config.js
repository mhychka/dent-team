var fs = require('fs');
var path = require('path');

const NODE_ENV = process.env.NODE_ENV || 'development'
var webpack = require('webpack')
var buildVersion = fs.readFileSync(path.join(__dirname, '../build.txt'), {encoding: 'utf-8'}).trim().replace(/\n$/, '');
//var ExtractTextPlugin = require ('extract-text-webpack-plugin');

var config = {
    addVendor: function (name, path) {
        this.resolve.alias[name] = path;
        this.module.noParse.push(new RegExp(path));
    },
    context:  __dirname + "/src",
    entry: {
        'i18n-uk': './i18n-uk.es6',
        'admin-inline-images': './admin-inline-images.es6',
        'app': './app.jsx'
    },
    output: {
        path: NODE_ENV == 'development' ? __dirname + "/static/js/bundles" : __dirname + "/static/js/compressed",
        filename: NODE_ENV == 'development' ? "[name].webpack.bundle.js" : "[name].compressed."+buildVersion+".bundle.js",
        libruary:"[name]"
    },
    externals: {
        "jquery": "jQuery",
    },
    resolve: {
        root: path.resolve(__dirname),
        alias:{
            static: 'static'
        },
        modulesDirectories: ['node_modules', 'src'],
    },
    watch: NODE_ENV == 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    devtool: NODE_ENV == 'development' ? "source-map" : null,
    plugins: [
        //new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV),
            'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
        }),
        //new ExtractTextPlugin('bundle.css'),
        new webpack.ContextReplacementPlugin(/node_modules\/moment\/locale/, /uk|de/),
    ],
    module: {
        loaders: [
            {
                test: /(\.es6|\.jsx)$/,
                exclude: /\/node_modules\//,
                loaders: ['babel-loader']
            }
        ]
    }
}

if(NODE_ENV == 'production'){
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true}))
}

module.exports = config