import * as types from 'constants/ActionTypes.es6';
import {TextsUrls} from 'constants/Urls.es6';
import {changeMetaKeywords} from 'actions/site.es6'


export function fetchCarouselImages(){
    return (dispatch, getState) => {
        dispatch(carouselImagesRequest())
        var url = `${TextsUrls.api.texts}?aliases=index-flexslider&fields=images`
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function (response) {
            var json = response.json()

            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            var text = response[0]
            var images = response[0] ? response[0].images : []
            dispatch(carouselImagesLoaded(images))
            dispatch(changeMetaKeywords(text.meta_keywords))
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function carouselImagesRequest() {
    return {
        type: types.INDEX_CAROUSEL_IMAGES_REQUEST,
    }
}

export function carouselImagesLoaded(images) {
    return {
        type: types.INDEX_CAROUSEL_IMAGES_LOADED,
        images
    }
}

export function fetchTeasers(){
    return (dispatch, getState) => {
        dispatch(carouselImagesRequest())
        var url = `${TextsUrls.api.texts}?aliases=index-teaser-1,index-teaser-2,index-teaser-3,index-teaser-4,index-teaser-5,index-teaser-6,index-teaser-7,index-teaser-8&fields=text_formatted`
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function (response) {
            var json = response.json()

            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            dispatch(teasersLoaded(response))
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function fetchImageTeasers(){
    return (dispatch, getState) => {
        dispatch(carouselImagesRequest())
        var url = `${TextsUrls.api.texts}?aliases=index-image-teaser&fields=images`
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function (response) {
            var json = response.json()

            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            dispatch(imageTeasersLoaded(response[0] ? response[0].images : []))
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function teasersRequest() {
    return {
        type: types.INDEX_TEASERS_REQUEST,
    }
}

export function teasersLoaded(teasers) {
    return {
        type: types.INDEX_TEASERS_LOADED,
        teasers
    }
}

export function imageTeasersLoaded(imageTeasers) {
    return {
        type: types.INDEX_IMAGE_TEASERS_LOADED,
        imageTeasers
    }
}

