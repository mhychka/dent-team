import * as types from 'constants/ActionTypes.es6';
import {pathMatch} from 'utils/url.es6'

export function environmentChangedSize(height, width) {
    return {
        type: types.ENVIRONMENT_CHANGED_SIZE,
        height,
        width
    };
}

export function determineClientDevice(height, width) {
    var device = 'desctop'
    if(width && width < 570){
        device = 'mobile'
    }else if(width && width > 570 && width < 1030){
        device = 'tablet'
    }

    return {
        type: types.ENVIRONMENT_DEVICE_CHANGED,
        device
    };
}

export function initEnvironment() {
    return dispatch => {
        dispatch(environmentChangedSize(window.innerHeight, window.innerWidth));
        dispatch(determineClientDevice(window.innerHeight, window.innerWidth));
        window.onresize = () => {
            dispatch(environmentChangedSize(window.innerHeight, window.innerWidth));
            dispatch(determineClientDevice(window.innerHeight, window.innerWidth));
        }
    };
}
