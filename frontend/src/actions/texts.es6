import * as types from 'constants/ActionTypes.es6';
import {TextsUrls} from 'constants/Urls.es6';
import {arrayOf, normalize} from 'normalizr';

export function fetchText(url){
    return (dispatch, getState) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function (response) {
            var json = response.json()
            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            dispatch(textLoaded(response))
        }).catch(function (error) {
            console.log(error)
        })
    }
}


export function fetchTextIfNeed(textAlias){
    return (dispatch, getState) => {
        var {texts} = getState()
        if(!texts.texts[textAlias]){
            var url = TextsUrls.api.text(textAlias)
            return dispatch(fetchText(url));
        }
    }
}

export function textLoaded(text) {
    return {
        type: types.TEXTS_TEXT_LOADED,
        text
    }
}