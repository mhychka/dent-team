import * as types from 'constants/ActionTypes.es6';
import {ArticlesUrls} from 'constants/Urls.es6';
import {arrayOf, normalize} from 'normalizr';
import {articleSchema} from 'constants/Schemas.es6'

export function fetchArticle(url){
    return (dispatch, getState) => {
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function (response) {
            var json = response.json()
            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            dispatch(articleLoaded(response))
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function fetchArticleIfNeed(articleAlias){
    return (dispatch, getState) => {
        return dispatch(fetchArticle(ArticlesUrls.api.article(articleAlias)));
    }
}

export function fetchArticles(url, articlesSet){
    return (dispatch, getState) => {
        dispatch(articlesRequest(articlesSet, url))
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function (response) {
            var json = response.json()

            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            const normalized = normalize(response.data, arrayOf(articleSchema));
            dispatch(articlesLoaded(articlesSet, normalized.entities.articles, normalized.result, response.keys.nextUrl))
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function fetchArticlesIfNeed(articlesSetId){
    return (dispatch, getState) => {
        const {articles} = getState();
        var articlesSets = articles.articlesSets;
        var activeArticlesSet = articlesSets[articlesSetId];
        var nextUrl = null

        if(!activeArticlesSet){
            nextUrl = ArticlesUrls.api.articles+articlesSetId
        } else if (!activeArticlesSet.isFetching && activeArticlesSet.nextUrl) {
            nextUrl = activeArticlesSet.nextUrl
        }

        if (nextUrl) {
            return dispatch(fetchArticles(nextUrl, articlesSetId));
        }
    }
}

export function fetchCategoriesIfNeed(){
    return (dispatch, getState) => {
        return dispatch(fetchCategories());
    }
}

export function fetchCategories(url=null) {
    return (dispatch, getState) => {
        if(!url){
            url =  ArticlesUrls.api.categories
        }
        dispatch(categoriesRequest())
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function(response){
            var json = response.json()

            if(!response.ok){
                return json.then(err => {throw err;});
            }else{
               return json
            }
        }).then(function(response){
            dispatch(categoriesLoaded(response))
        }).catch(function(error){
            console.log(error)
        })
    }
}

export function fetchTagsIfNeed(){
    return (dispatch, getState) => {
        return dispatch(fetchTags());
    }
}

export function fetchTags(url=null) {
    return (dispatch, getState) => {
        if(!url){
            url =  ArticlesUrls.api.tags
        }
        return fetch(url, {
            method: 'GET',
            headers: {
                'Content-type': `application/json`,
                'Accept': 'application/json, text/plain, */*'
            },
            credentials: 'include'
        }).then(function(response){
            var json = response.json()

            if(!response.ok){
                return json.then(err => {throw err;});
            }else{
               return json
            }
        }).then(function(response){
            dispatch(tagsLoaded(response))
        }).catch(function(error){
            console.log(error)
        })
    }
}

export function articlesRequest(articlesSet, nextUrl) {
    return {
        type: types.ARTICLES_ARTICLES_REQUEST,
        articlesSet,
        nextUrl
    }
}

export function articlesLoaded(articlesSet, articles, items, nextUrl) {
    return {
        type: types.ARTICLES_ARTICLES_LOADED,
        articlesSet,
        articles,
        items,
        nextUrl
    }
}

export function articleLoaded(article) {
    return {
        type: types.ARTICLES_ARTICLE_LOADED,
        article
    }
}

export function categoriesRequest() {
    return {
        type: types.ARTICLES_CATEGORIES_REQUEST,
    }
}

export function categoriesLoaded(categories) {
    return {
        type: types.ARTICLES_CATEGORIES_LOADED,
        categories
    }
}

export function tagsLoaded(tags) {
    return {
        type: types.ARTICLES_TAGS_LOADED,
        tags
    }
}

