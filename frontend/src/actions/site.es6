import * as types from 'constants/ActionTypes.es6';
import * as config from 'constants/Config.es6';
import {I18nUrls, ContactsUrls} from 'constants/Urls.es6';
import {makeQueryString} from 'utils/url.es6'
import {getCookie} from 'utils/cookie.es6'


export function changeLanguage(params){
    return (dispatch, getState) => {
        var url = I18nUrls.api.language
        params = makeQueryString(params)

        return fetch(url, {
            method: 'PUT',
            headers: {
                'Content-type': `application/x-www-form-urlencoded; charset=UTF-8;`,
                'Accept': 'application/json, text/plain, */*',
                'X-CSRFToken': getCookie('csrftoken')
            },
            credentials: 'include',
            body: params
        }).then(function (response) {
            var json = response.json()
            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            location.reload()
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function initSite() {
    return dispatch => {
        dispatch(setLanguage(document.documentElement.lang));
    };
}

export function createContactMessage(params, callback=null){
    return (dispatch, getState) => {
        var url = ContactsUrls.api.message
        params = makeQueryString(params)

        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-type': `application/x-www-form-urlencoded; charset=UTF-8;`,
                'Accept': 'application/json, text/plain, */*',
                'X-CSRFToken': getCookie('csrftoken')
            },
            credentials: 'include',
            body: params
        }).then(function (response) {
            var json = response.json()
            if (!response.ok) {
                return json.then(err => {
                    throw err;
                });
            } else {
                return json
            }
        }).then(function (response) {
            dispatch(contactMessageSent())
            if(typeof callback == 'function'){
                callback(response)
            }
        }).catch(function (error) {
            console.log(error)
        })
    }
}

export function changeTitle(title) {
    document.title = `${config.SITE_NAME} | ${title}`

    return {
        type: types.SITE_TITLE_CHANGED,
        title
    }
}

export function changeMetaKeywords(value){
    return function (dispatch, getState) {
        $('meta[name=keywords]').attr('content', value);
    }
}

export function contactMessageSent(contactMessage) {
    return {
        type: types.SITE_CONTACTMESSAGE_SENT,
        contactMessage
    }
}

export function setLanguage(lang) {
    return {
        type: types.SITE_LANGUAGE,
        lang
    };
}

export function scrollToTop() {
    $("html, body").animate({scrollTop: 0}, 1000);
}