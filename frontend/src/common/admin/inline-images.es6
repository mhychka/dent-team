require('script!static/js/jquery/plugins/fileupload/jquery.ui.widget.js')
require('script!static/js/jquery/plugins/fileupload/load-image.all.min.js')
require('script!static/js/jquery/plugins/fileupload/canvas-to-blob.js')
require('script!static/js/jquery/plugins/fileupload/jquery.iframe-transport.js')
require('script!static/js/jquery/plugins/fileupload/jquery.fileupload.js')
require('script!static/js/jquery/plugins/fileupload/jquery.fileupload-process.js')
require('script!static/js/jquery/plugins/fileupload/jquery.fileupload-image.js')
require('script!static/js/crlf.js')

class AdminInlineImagesManager{
    constructor(options) {
        this.$box = $(options.box);
        this.$progress = this.$box.find('#fileupload-progress')
        this.$input = this.$box.find('#fileupload-input')
        this.$errors = this.$box.find('#fileupload-errors')
        this.uploadUrl = options.uploadUrl
        this.uploadconf = options.uploadconf
        this.parentId = options.parentId
    }

    showProgress(data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        this.$progress.find('.bar').css('width', progress + '%');
    }

    run() {
        var self = this;
        self.bindButtons()
    }

    bindButtons(){
        var self = this;

        self.$input.fileupload({
            add: (e, data) => {
                var error

                data.url = self.uploadUrl
                data.formData = {parent_id:self.parentId}

                for(let file of data.files) {
                    if (file.type.length && !self.uploadconf.accept_file_types.test(file.type)) {
                        error = gettext('Not an accepted file type: ') + file.type
                    }
                    if (file.name.length && !self.uploadconf.accept_file_extensions.test(file.name)) {
                        error = gettext('Not an accepted filename: ') + file.name
                    }
                    if (file.size && file.size > self.uploadconf.max_file_size) {
                        error = gettext('Filesize is too big. Allowed ') + FileUtils.bytesToSize(self.uploadconf.max_file_size)
                    }

                    if (error) {
                        self.$errors.addClass('has-error').text(error)
                    } else {
                        data.submit()
                    }
                }
            },
            progress: (e, data) => {
                var self = this;
                var progress = parseInt(data.loaded / data.total * 100, 10)
                self.$progress.find('.progress-bar').attr({
                    style: 'width:' + progress + '%',
                    'aria-valuenow': progress
                })
            },
            done: (e, data) => {
                location.reload()
            },
            dataType: 'json',
            singleFileUploads: true,
            autoUpload: true

        })
    }
}

module.exports = {
    AdminInlineImagesManager: AdminInlineImagesManager
}