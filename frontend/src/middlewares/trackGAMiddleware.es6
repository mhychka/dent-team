export default function trackGAMiddleware({ getState }) {
  return (next) =>
        (action) => {
            if (action && action.type == '@@router/LOCATION_CHANGE' && NODE_ENV == 'production'){
                ga('send', 'pageview', action.payload.pathname);
            }
        return next(action);
    };
}