import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {TextsUrls} from 'constants/Urls.es6';
import {makeQueryString} from 'utils/url.es6';
let moment = require('moment')
import {Link} from 'react-router'
import {changeTitle, changeMetaKeywords} from 'actions/site.es6'
import {pathMatch} from 'utils/url.es6'


class Text extends Component {
    componentDidMount(){
        const {dispatch, text, environment} = this.props;

        $('.article-text a').click(function (event) {
            var href = $(this).attr('href')
            if(pathMatch(href, '/app/')){
                event.preventDefault()
                appHistory.push(href)
                return false
            }
            return true
        })

        dispatch(changeTitle(text.title))
        dispatch(changeMetaKeywords(text.meta_keywords))
    }

    render() {
        const {text, environment} = this.props;

        return (
            <article>
                <div className="post-image">
                    <div className="post-heading">
                        <h3><a href="#">{text.title}</a></h3>
                    </div>
                </div>

                <div className="article-text" dangerouslySetInnerHTML={{__html: text.text_formatted}}></div>
            </article>
        );
    }
}

Text.propTypes = {
    text: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    const {environment} = state;
    return {
        environment
    };
}

export default connect(mapStateToProps)(Text);
