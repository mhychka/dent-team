import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {GoogleMapLoader, GoogleMap, Marker} from "react-google-maps";
import Loading from 'components/Loading.jsx'

class ContactsAddress extends Component {
    render(){
        const {dispatch} = this.props;

        return (
            <div className="contacts vcard">
                <a className="org contact-link" href="javascript://"><strong>Dent Team</strong></a>
                <a className="adr contact-link" href="javascript://"><span className="glyphicon glyphicon-home"> </span>&nbsp;Украiна, м.Умань<br />вул. Залізняка 2-е</a>
                <a className="email contact-link" href="mailto:info@dentteam.pp.ua"><span className="glyphicon glyphicon-envelope"> </span>&nbsp;info@dentteam.pp.ua</a>
            </div>
        )
    }
}

function mapStateToProps(state, props) {
    const {environment} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(ContactsAddress);
