import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'
import {MainUrls} from 'constants/Urls.es6';
import {activeClass} from 'utils/url.es6'


class HeaderDesctop extends Component {
    render(){
        return (
            <header>
                <div className="navbar navbar-default navbar-desctop navbar-fixed-top">
                    <div className="container">
                        <div className="navbar-header">
                            <Link className="navbar-brand" to={MainUrls.index} ><img width="170px" src="/static/img/logo.png" /></Link>
                        </div>
                        <div className="navbar-collapse collapse">
                            <ul className="nav navbar-nav">
                                <li className={activeClass(location.pathname, '/app/', 'active', true)}>
                                    <Link to={MainUrls.index} >{gettext('Home')}</Link>
                                </li>
                                <li className={activeClass(location.pathname, '/text/about')}>
                                    <Link to={MainUrls.text('about')} >{gettext('About Us')}</Link>
                                </li>
                                <li className={activeClass(location.pathname, '/text/services') || activeClass(location.pathname, '/app/articles')}>
                                    <Link to={MainUrls.text('services')} >{gettext('Services')}</Link>
                                </li>
                                <li className={activeClass(location.pathname, '/text/prices')}>
                                    <Link to={MainUrls.text('prices')} >{'Цiни'}</Link>
                                </li>
                                <li className={activeClass(location.pathname, '/contacts')}>
                                    <Link to={MainUrls.contacts} >{gettext('Contacts')}</Link>
                                </li>
                            </ul>
                            <div className="clear"></div>
                            <div className="header-contacts vcard">
                                <a className="tel contact-link" href="tel:0634984626"><span className="glyphicon glyphicon-phone"> </span>&nbsp;0634984626 </a>
                                &nbsp;&nbsp;
                                <a className="tel contact-link" href="tel:0976919636"><span className="glyphicon glyphicon-phone"> </span>&nbsp;0976919636 </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

HeaderDesctop.propTypes = {

}

function mapStateToProps(state, props) {
    return {};
}

export default connect(mapStateToProps)(HeaderDesctop);
