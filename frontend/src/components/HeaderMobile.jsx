import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'
import {ArticlesUrls, MainUrls} from 'constants/Urls.es6';
import {activeClass} from 'utils/url.es6'
import * as config from 'constants/Config.es6';

class HeaderMobile extends Component {
    render(){
        const {site} = this.props

        return (
            <header className="header-mobile">
                <div className="navbar navbar-default navbar-mobile navbar-fixed-top">
                    <div className="container">
                        <button type="button" className="navbar-toggle menu-open-btn menu-toggle-btn">
                            <span className="icon-bar"> </span>
                            <span className="icon-bar"> </span>
                            <span className="icon-bar"> </span>
                        </button>
                        <h4>{config.SITE_NAME}</h4>
                        <div className="mini-logo">
                            <img src="/static/img/favicon/apple-icon-72x72.png" alt="" />
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

HeaderMobile.propTypes = {

}

function mapStateToProps(state, props) {
    const {site} = state

    return {
        site
    };
}

export default connect(mapStateToProps)(HeaderMobile);
