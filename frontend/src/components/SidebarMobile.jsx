import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'
import {ArticlesUrls, MainUrls} from 'constants/Urls.es6';
import {activeClass} from 'utils/url.es6'
import SocialButtons from 'components/SocialButtons.jsx'

class SidebarMobile extends Component {
    componentDidMount(){
        const {environment} = this.props;

        window.slideout = new Slideout({
            panel: document.getElementById('panel'),
            menu: document.getElementById('sidebar'),
            padding: 256,
            tolerance: 70,
            touch: false
        });

        $('.menu-toggle-btn').click(() => {
            slideout.toggle();
            return false
        })
    }

    componentWillUnmount(){
        $(".menu-toggle-btn").unbind('click')
        window.slideout.destroy()
    }

    componentWillReceiveProps(nextProps){
        const {location, articles} = this.props;
        if(location.pathname != nextProps.location.pathname && window.slideout){
            window.slideout.close()
        }
    }

    render(){
        const {location} = this.props
        return (
            <div id="sidebar">
                <header>{gettext('Menu')}</header>
                <ul>
                    <li className={activeClass(location.pathname, '/app', 'active', true)}>
                        <Link to={MainUrls.index} >{gettext('Home')}</Link>
                    </li>
                    <li className={activeClass(location.pathname, '/text/about')}>
                        <Link to={MainUrls.text('about')} >{gettext('About Us')}</Link>
                    </li>
                    <li className={activeClass(location.pathname, '/text/services')}>
                        <Link to={MainUrls.text('services')} >{gettext('Services')}</Link>
                    </li>
                    <li className={activeClass(location.pathname, '/text/prices')}>
                        <Link to={MainUrls.text('prices')} >{'Цiни'}</Link>
                    </li>
                    <li className={activeClass(location.pathname, '/contacts')}>
                        <Link to={MainUrls.contacts} >{gettext('Contacts')}</Link>
                    </li>
                </ul>
                <header>{'Сoцмережi'}</header>
                <li className={activeClass(location.pathname, '/contacts')}>
                     <SocialButtons />
                </li>
            </div>
        );
    }
}

SidebarMobile.propTypes = {
    location: PropTypes.object.isRequired,
    articles: PropTypes.object.isRequired,
    slideout: PropTypes.object
}

function mapStateToProps(state, props) {
    const {environment, routing, articles} = state;

    return {
        environment,
        location: routing.locationBeforeTransitions,
        articles
    };
}

export default connect(mapStateToProps)(SidebarMobile);
