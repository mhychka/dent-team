import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {GoogleMapLoader, GoogleMap, Marker} from "react-google-maps";
import Loading from 'components/Loading.jsx'
import ContactsAddress from 'components/ContactsAddress.jsx'
import ContactsPhones from 'components/ContactsPhones.jsx'

class Contacts extends Component {
    render(){
        const {dispatch} = this.props;

        return (
            <div className="contacts">
                <div className="contacts-text contacts-text-uk row">
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <ContactsAddress />
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <br/>
                        <ContactsPhones />
                    </div>
                </div>

                <div className="location">
                    <iframe className="google-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1315.0950019577203!2d30.202690970446113!3d48.75916774991195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x49dc9b00e85f9d93!2z0KHRgtC-0LzQsNGC0L7Qu9C-0LPRltGPICLQpNC10L3RltC60YEiLiDQmtCw0LHRltC90LXRgiBEZW50VGVhbSDQu9GW0LrQsNGA0ZbQsi3RgdGC0L7QvNCw0YLQvtC70L7Qs9GW0LIg0JDQvdC90Lgg0JzRg9GB0LDRgtC-0LLQvtGXINGC0LAg0JLRltGC0LDQu9GW0Y8g0JzQuNGA0L7RiNC90LjRh9C10L3QutCw!5e0!3m2!1sru!2sua!4v1464694330299"></iframe>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, props) {
    const {environment} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(Contacts);
