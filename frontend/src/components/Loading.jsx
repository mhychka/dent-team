import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

class LoadingDiv extends Component {
    render(){
        return (
            <div className="loading"><img className="loading" src="/static/img/loading.gif" /></div>
        );
    }
}

function mapStateToProps(state, props) {
    return {}
}

export default connect(mapStateToProps)(LoadingDiv);
