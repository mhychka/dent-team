import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'


class Teasers extends Component {
    render(){
        const {dispatch, teasers} = this.props;
        if(!teasers){
            return <Loading />
        }
        var data = []
        var i = 1
        for(let teaser of teasers){
            data.push(
                <div className="col-lg-3" key={`${i}-${teaser.id}`}>
                    <div className="box">
                        <div className="box-gray aligncenter">
                            <h4>{teaser.title}</h4>
                            <div className="icon">
								<i className="fa fa-edit fa-3x"> </i>
                            </div>
                            <p className="teaser-preview" dangerouslySetInnerHTML={{__html: teaser.preview}}></p>
                        </div>
                        <div className="box-bottom">
                            <Link to={teaser.reference}>{teaser.ref_title}</Link>
                        </div>
                    </div>
                </div>
            )
            i += 1
        }

        return <div className="row teasers">{data}</div>
    }
}

Teasers.propTypes = {
    teasers: PropTypes.array.isRequired,
}

function mapStateToProps(state, props) {
    const {environment} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(Teasers);
