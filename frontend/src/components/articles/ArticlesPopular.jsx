import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {ArticlesUrls} from 'constants/Urls.es6';
import Loading from 'components/Loading.jsx'


class ArticlesPopular extends Component {
    renderItems(){
        const {articlesSet, articles} = this.props;

        if(!articlesSet.items || !articlesSet.items.length){
            return <Loading />
        }

        var items = articlesSet.items
        var data = []
        for(let articleId of items){
            var article = articles[articleId]
            var imgSrc = article.images.length ? article.images[0].thumbs.s.src : '/static/img/thumb1.jpg'
            data.push(
                <li key={article.id}>
                    <img src={imgSrc} className="pull-left" alt="" />
                    <h6>
                        <Link to={ArticlesUrls.article(article.alias)}>{article.ref_title}</Link>
                    </h6>
                    <p>{article.ref_preview}</p>
                </li>
            )
        }

        return data
    }

    render() {
        return (
            <div className="widget">
                <h5 className="widgetheading">{gettext('Popular')}</h5>
                <ul className="recent">
                    {this.renderItems()}
                </ul>
            </div>
        );
    }
}

ArticlesPopular.propTypes = {
    articlesSet: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    const {environment, articles} = state;
    return {
        environment,
        articles: articles.articles
    };
}

export default connect(mapStateToProps)(ArticlesPopular);
