import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import ArticleListItem from 'components/articles/ArticleListItem.jsx'
import InfiniteScroll from 'redux-infinite-scroll';
import Loading from 'components/Loading.jsx'
import {fetchArticlesIfNeed} from 'actions/articles.es6'


class ArticlesList extends Component {
    renderArticles(){
        const {dispatch, articlesSet, articles} = this.props;
        const items = articlesSet ? articlesSet.items : [];
        var data = []
        var i = 1
        for(let articleId of items){
            let article = articles[articleId]
            let key = `${i}-${articleId}`
            data.push(<ArticleListItem key={key} article={article} />)
            i += 1
        }
        return data
    }

    loadArticles(){
        const {dispatch,  articlesSet, articlesSetId} = this.props;
        dispatch(fetchArticlesIfNeed(articlesSetId))
    }

    render() {
        const {dispatch, articlesSet} = this.props;
        var loadingMore = articlesSet.isFetching
        return <InfiniteScroll loadingMore={loadingMore} loader={<Loading/>} elementIsScrollable={false} items={this.renderArticles()} loadMore={this.loadArticles.bind(this)}  />
    }
}

ArticlesList.propTypes = {
    articlesSetId: PropTypes.string.isRequired,
    articlesSet: PropTypes.object.isRequired,
    articles: PropTypes.object.isRequired,
}

function mapStateToProps(state, props) {
    const {environment} = state;
    return { environment};
}

export default connect(mapStateToProps)(ArticlesList);
