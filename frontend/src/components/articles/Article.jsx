import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {articlesLoaded} from 'actions/articles.es6';
import {ArticlesUrls} from 'constants/Urls.es6';
import {makeQueryString} from 'utils/url.es6';
let moment = require('moment')
import Loading from 'components/Loading.jsx'
import {Link} from 'react-router'
import {changeTitle, changeMetaKeywords} from 'actions/site.es6'
import {pathMatch} from 'utils/url.es6'

class Article extends Component {
    componentDidMount(){
        const {dispatch, article, environment, site} = this.props;

        $('.article-box a').click(function (event) {
            var href = $(this).attr('href')
            if(pathMatch(href, '/app/')){
                event.preventDefault()
                appHistory.push(href)
                return false
            }
            return true
        })

        dispatch(changeTitle(article.title))
        dispatch(changeMetaKeywords(article.meta_keywords))

    }

    renderCategory(){
        const {article, environment} = this.props;
        var category = article.category

        if(category){
            return (
                <li className="meta-post-item" key="meta-post-cat">
                    <span key="cat-name">{gettext('Category')}: </span><Link to={ArticlesUrls.articlesCategory(category.alias)}>{category.title}</Link>
                </li>
            )
        }
    }

    renderTags(){
        const {article, environment} = this.props;
        var tags = article.tags

        if(tags.length){
            var data = [<span key="tags-name">{gettext('Tags')}: </span>]
            for(let tag of tags){
                data.push(<Link key={tag.id} to={ArticlesUrls.articlesTag(tag.codename)}>{tag.title}</Link>)
                data.push(<span className="comma" key={`${tag.id}-comma`}>, </span>)
            }
            return (
                <li className="meta-post-item" key="meta-post-tags">{data}</li>
            )
        }
    }

    renderHeader(){
        //<div className="top-article">
        //    <ul className="meta-post">
        //        <li className="meta-post-item" key="meta-post-date">
        //            <i className="icon-calendar"> </i>{moment(article.created).locale(site.language).format('LL')}
        //        </li>
        //    </ul>
        //</div>
    }

    render() {
        const {article, environment, site} = this.props;
        return (
            <article className="article-box">
                <div className="post-image">
                    <div className="post-heading">
                        <h3><a href="#">{article.title}</a></h3>
                    </div>
                </div>

                {this.renderHeader()}
                <div className="article-preview" dangerouslySetInnerHTML={{__html: article.preview_formatted}}></div>
                <div className="article-text" dangerouslySetInnerHTML={{__html: article.text_formatted}}></div>
            </article>
        );
    }
}

Article.propTypes = {
    article: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    const {environment, site} = state;
    return {
        environment,
        site
    };
}

export default connect(mapStateToProps)(Article);
