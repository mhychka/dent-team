import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {ArticlesUrls} from 'constants/Urls.es6';
import Loading from 'components/Loading.jsx'


class ArticlesCategories extends Component {
    renderCategories(){
        const {categories} = this.props;
        var data = []
        for(let category of categories){
            data.push(<li key={category.id}><i className="icon-angle-right"> </i><Link activeClassName="active" to={ArticlesUrls.articlesCategory(category.alias)}>{category.title}</Link></li>)
        }
        if(!data.length){
            return <Loading />
        }
        return data
    }

    render() {
        return (
            <div className="widget">
                <h5 className="widgetheading">{gettext('Categories')}</h5>
                <ul className="cat">
                    {this.renderCategories()}
                </ul>
            </div>
        );
    }
}

ArticlesCategories.propTypes = {
    categories: PropTypes.array.isRequired
}

function mapStateToProps(state) {
    const {environment} = state;
    return {
        environment
    };
}

export default connect(mapStateToProps)(ArticlesCategories);
