import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {ArticlesUrls} from 'constants/Urls.es6';
import Loading from 'components/Loading.jsx'


class ArticlesTags extends Component {
    renderTags(){
        const {tags} = this.props;

        if(!tags.length){
            return <Loading />
        }

        var data = []
        for(let tag of tags){
            data.push(<li key={tag.id}><Link activeClassName="active" to={ArticlesUrls.articlesTag(tag.codename)}>{tag.title}</Link></li>)
        }

        return data
    }

    render() {
        return (
            <div className="widget">
                <h5 className="widgetheading">{gettext('Tags')}</h5>
                <ul className="tags">
                    {this.renderTags()}
                </ul>
            </div>
        );
    }
}

ArticlesTags.propTypes = {
    tags: PropTypes.array.isRequired
}

function mapStateToProps(state) {
    const {environment} = state;
    return {
        environment
    };
}

export default connect(mapStateToProps)(ArticlesTags);
