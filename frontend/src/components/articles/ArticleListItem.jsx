import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {articlesLoaded} from 'actions/articles.es6';
import {ArticlesUrls} from 'constants/Urls.es6';
import {makeQueryString} from 'utils/url.es6';
import {Link} from 'react-router'
let moment = require('moment')

class ArticleListItem extends Component {
    renderCategory(){
        const {article, environment} = this.props;
        var category = article.category

        if(category){
            return (
                <li className="meta-post-item" key="meta-post-cat">
                    <span key="cat-name">{gettext('Category')}: </span><Link to={ArticlesUrls.articlesCategory(category.alias)}>{category.title}</Link>
                </li>
            )
        }
    }

    render() {
        var self = this
        const {article, environment} = self.props;
        return (
            <article>
                <div className="post-image">
                    <div className="post-heading">
                        <h3><Link to={ArticlesUrls.article(article.alias)}>{article.title}</Link></h3>
                    </div>
                </div>
                <div className="preview" dangerouslySetInnerHTML={{__html: article.preview_formatted}}></div>
                <div className="bottom-article">
                    <ul className="meta-post">
                        <li className="meta-post-item" key="meta-post-date">
                            <i className="icon-calendar"> </i>{moment(article.created).locale(environment.lang).format('LL')}
                        </li>
                        {this.renderCategory()}
                    </ul>
                    <Link to={ArticlesUrls.article(article.alias)} className="pull-right">{gettext('Continue reading')} <i className="icon-angle-right"> </i></Link>
                </div>
            </article>
        );
    }
}

ArticleListItem.propTypes = {
    article: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    const {environment} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(ArticleListItem);
