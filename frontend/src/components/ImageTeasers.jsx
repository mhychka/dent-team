import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'


class ImageTeasers extends Component {
    componentDidMount(){
        const {dispatch, cId} = this.props

        $(`.fancybox-${cId}`).fancybox({
            padding: 0,
            autoResize: true,
            arrows: false,
            closeBtn: false,
            nextClick: true,
            beforeShow: function () {
                this.title = $(this.element).attr('title');
                this.title = '<h4>' + this.title + '</h4>' + '<p>' + $(this.element).parent().find('img').attr('alt') + '</p>';
            },
            helpers: {
                title: null,
                buttons	: {}
            }
        });
    }

    renderImages(){
        const {dispatch, images, cId} = this.props
        var className = `hover-wrap fancybox-${cId}`

        var data = []
        for(let image of images){
            data.push(
                <li key={`fancybox-image-${image.id}`} className="col-lg-3 design" data-id="id-0" data-type="web">
                    <div className="item-thumbs">
                        <a className={className} data-fancybox-group="gallery"  title={image.title ? image.title : ''} href={image.thumbs.m2.src}>
                            <span className="overlay-img"> </span>
                            <span className="overlay-img-thumb font-icon-plus"> </span>
                        </a>
                        <img src={image.thumbs.m2.src} alt={image.description ? image.description : ''} />
                    </div>
                </li>
            )
        }

        return data
    }

    render(){
        const {dispatch, cId} = this.props


        return (
            <div>
                <h4 className="heading">{this.props.title}</h4>
                <div className="row">
                    <section id="projects">
                        <ul id="thumbs" className="portfolio">
                            {this.renderImages()}
                        </ul>
                    </section>
                </div>
            </div>
        )
    }
}

ImageTeasers.propTypes = {
    images: PropTypes.array.isRequired,
    cId: PropTypes.string.isRequired,
    title: PropTypes.string
}

function mapStateToProps(state, props) {
    const {environment} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(ImageTeasers);
