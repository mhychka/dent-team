import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {GoogleMapLoader, GoogleMap, Marker} from "react-google-maps";
import Loading from 'components/Loading.jsx'
import {changeLanguage} from 'actions/site.es6'
import * as config from 'constants/Config.es6';

class LangSwitcher extends Component {
    componentDidMount(){
        var {dispatch, language} = this.props

        $('.bfh-selectbox-options li a').click(function(){
            var $btn = $(this)
            var lang = $btn.data('option')
            var currLang = $('.bfh-languages input').val()
            if(lang && lang.length && lang != currLang ){
                dispatch(changeLanguage({'lang':lang.split('_')[0]}))
            }
            return false
        })
        $('.bfh-selectbox-toggle').click(function(){
            $('.bfh-selectbox-options').toggle()
        })
    }

    languageCode(language){
        if(language == 'de'){
            return 'de_DE'
        }else if(language == 'uk'){
            return 'uk_UA'
        }
        return ''
    }

    languageCodes(){
        var codes = []
        for(let lang of config.SITE_LANGUAGES){
            codes.push(this.languageCode(lang))
        }
        return codes
    }

    renderLanguageslinks(){
        var data = []
        for(let lang of config.SITE_LANGUAGES){
            let code = this.languageCode(lang)
            data.push(<li key={`lang-${code}`}><a tabIndex="-1" href="#" data-option={code}><i className={`glyphicon bfh-flag-${code.split('_')[1]}`}> </i>{code.split('_')[1]}</a></li>)
        }
        return data
    }

    render(){
        var {dispatch, language} = this.props
        var languageCode = this.languageCode(language)
        var languageCodes = this.languageCodes()

        return (
            <div className="bfh-selectbox bfh-languages" data-language={languageCode} data-available={languageCodes.join(',')} data-flags="true">
                <a className="bfh-selectbox-toggle form-control" role="button" data-toggle="bfh-selectbox" href="#">
                    <span className="bfh-selectbox-option"><i className={`glyphicon bfh-flag-${languageCode.split('_')[1]}`}> </i>{languageCode.split('_')[1]}</span><span className="caret selectbox-caret"> </span>
                </a>
                <div className="bfh-selectbox-options">
                    <div role="listbox">
                        <ul role="option">
                            {this.renderLanguageslinks()}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, props) {
    const {environment, site} = state;

    return {
        environment,
        language: site.language
    };
}

export default connect(mapStateToProps)(LangSwitcher);
