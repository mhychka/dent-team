import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {GoogleMapLoader, GoogleMap, Marker} from "react-google-maps";
import Loading from 'components/Loading.jsx'

class ContactsPhones extends Component {
    render(){
        const {dispatch} = this.props;

        return (
            <div className="contacts vcard">
                <a className="tel contact-link" href="tel:0634984626"><span className="glyphicon glyphicon-phone"> </span>&nbsp;0634984626 </a>
                <a className="tel contact-link" href="tel:0976919636"><span className="glyphicon glyphicon-phone"> </span>&nbsp;0976919636 </a>
            </div>
        )
    }
}

function mapStateToProps(state, props) {
    const {environment} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(ContactsPhones);
