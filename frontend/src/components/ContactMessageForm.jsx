import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import {reduxForm, reset} from 'redux-form';
import {createValidator, required, minLength, email} from 'utils/validation.es6';
import {createContactMessage} from 'actions/site.es6'
import * as types from 'constants/ActionTypes.es6';
import InlineNotification from 'react-redux-notifications/dist-modules/InlineNotification.js'


const validate = createValidator({
    name: [required, minLength(3)],
    subject: [required],
    email: [required, email],
    message: [required],
});


class ContactMessageForm extends Component {

    submitForm(values){
        const {dispatch} = this.props
        dispatch(createContactMessage(values, (contactMessage) => {
            dispatch(reset('contactmessage'))
        }))
    }

    render() {
        const {dispatch} = this.props;
        const {fields: {name, email, subject, message}, handleSubmit, submitting, valid, pristine} = this.props;

        return (
            <div className="contacts-form">
                <h4>{gettext('Contact Us')}</h4>
                <form id="contactform" action="contact/contact.php" method="post" className="validateform" name="send-contact">
                    <div className="row">
                        <div className="col-lg-4 field">
                            <input type="text" placeholder={gettext('* Enter your full name')} {...name} />
                            {name.touched && name.error && <div className="validation">{name.error}</div>}
                        </div>
                        <div className="col-lg-4 field">
                            <input type="text" placeholder={gettext('* Enter your email address')} {...email} />
                            {email.touched && email.error && <div className="validation">{email.error}</div>}
                        </div>
                        <div className="col-lg-4 field">
                            <input type="text" placeholder={gettext('* Enter your subject')} {...subject} />
                            {subject.touched && subject.error && <div className="validation">{subject.error}</div>}
                        </div>
                        <div className="col-lg-12 margintop10 field">
                            <textarea rows="12" className="input-block-level" placeholder={gettext('Your message here...')} {...message} />
                            {message.touched && message.error && <div className="validation">{message.error}</div>}
                            <InlineNotification defaultMessage={gettext('Your message have been sent. We will contact you!')} triggeredBy={types.SITE_CONTACTMESSAGE_SENT} hideAfter={3500} />
                            <p>
                                <button className="btn btn-theme margintop10 pull-left" type="button" onClick={handleSubmit(this.submitForm.bind(this))}>{gettext('Submit message')}</button>
                                <span className="pull-right margintop20">{gettext('* Please fill all required form field, thanks!')}</span>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

ContactMessageForm.propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired
}

function mapStateToProps(state, props) {
    const {environment} = state;

    return {
        environment,
    };
}

export default reduxForm({
    form: 'contactmessage',
    fields: ['name', 'email', 'subject', 'message'],
    validate
})(ContactMessageForm)