import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'
import ContactsAddress from 'components/ContactsAddress.jsx'
import ContactsPhones from 'components/ContactsPhones.jsx'
import {activeClass} from 'utils/url.es6'
import {MainUrls, ArticlesUrls} from 'constants/Urls.es6'


class Footer extends Component {
    render(){
        return (
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="widget">
                                <h5 className="widgetheading">{gettext('Contacts')}</h5>
                                <ContactsAddress />
                                <ContactsPhones />
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="widget">
                                <h5 className="widgetheading">{gettext('Pages')}</h5>
                                <ul className="link-list">
                                    <li className={activeClass(location.pathname, '/app/', 'active', true)}>
                                        <Link to={MainUrls.index} >{gettext('Home')}</Link>
                                    </li>
                                    <li className={activeClass(location.pathname, '/text/about')}>
                                        <Link to={MainUrls.text('about')} >{gettext('About Us')}</Link>
                                    </li>
                                    <li className={activeClass(location.pathname, '/text/services')}>
                                        <Link to={MainUrls.text('services')} >{gettext('Services')}</Link>
                                    </li>
                                    <li className={activeClass(location.pathname, '/text/prices')}>
                                        <Link to={MainUrls.text('prices')} >{'Цiни'}</Link>
                                    </li>
                                    <li className={activeClass(location.pathname, '/contacts', 'active')}>
                                        <Link to={MainUrls.contacts} >{gettext('Contacts')}</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="widget">
                                <h5 className="widgetheading">{gettext('Services')}</h5>
                                <ul className="link-list">
                                    <li>
                                        <Link to={ArticlesUrls.article('vidbiluvannya')} >{'Відбілювання'}</Link>
                                    </li>
                                    <li>
                                        <Link to={ArticlesUrls.article('caries')} >{'Лікування карієсу'}</Link>
                                    </li>
                                    <li>
                                        <Link to={ArticlesUrls.article('root')} >{'Лікування каналiв'}</Link>
                                    </li>
                                    <li>
                                        <Link to={ArticlesUrls.article('hudoznia-restavracia')} >{'Реставрація'}</Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-3">
                            <div className="widget">
                                <h5 className="widgetheading">&nbsp;</h5>
                                <ul className="link-list">
                                    <li>
                                        <Link to={ArticlesUrls.article('profilactika')} >{'Профілактика стоматологічних захворювань'}</Link>
                                    </li>
                                    <li>
                                        <Link to={ArticlesUrls.article('neznimne')} >{'Непрямі реставрації'}</Link>
                                    </li>
                                    <li>
                                        <Link to={ArticlesUrls.article('znimne-protezyvannya')} >{'Знімне протезування'}</Link>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6">
                                <div className="copyright">
                                    <p>
                                        © Dent Team 2016. {gettext('All right reserved')}.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

function mapStateToProps(state, props) {
    const {environment, routing, articles} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(Footer);
