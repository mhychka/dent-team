import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'


class SocialButtons extends Component {
    render(){
        return (
            <div className="social">
                 <a target="_blank" href="https://plus.google.com/105284179679018008376" className="btn btn-social-icon btn-google">
                    <span className="fa fa-google"> </span>
                  </a>
                <a target="_blank" href="https://www.facebook.com/groups/DentTeamUman" className="btn btn-social-icon btn-facebook">
                    <span className="fa fa-facebook"> </span>
                  </a>
                <a target="_blank" href="https://vk.com/dentteamuman" className="btn btn-social-icon btn-vk">
                    <span className="fa fa-vk"> </span>
                  </a>
                <a target="_blank" href="https://www.instagram.com/dentteamuman/" className="btn btn-social-icon btn-instagram">
                    <span className="fa fa-instagram"> </span>
                  </a>
            </div>
        )
    }
}

function mapStateToProps(state, props) {
    const {environment, routing, articles} = state;

    return {
        environment,
    };
}

export default connect(mapStateToProps)(SocialButtons);
