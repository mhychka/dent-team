import * as types from '../constants/ActionTypes.es6';
import merge from 'lodash/object/merge';

const initialState = {
    texts: {},
};

export default function texts(previousState = initialState, action) {
    switch(action.type) {
        case types.TEXTS_TEXT_LOADED:
            return Object.assign({}, previousState, {texts: merge({}, previousState.texts, {[action.text.alias]: action.text})})

        default:
            return previousState;
    }
}