import * as types from '../constants/ActionTypes.es6';
import merge from 'lodash/object/merge';

const articlesInitialState = {
    articles: {},
    tags: [],
    article: null,
    categories: [],
    articlesSets: {}
};

const articlesSetInitialState = {
    isFetching: false,
    items: [],
    nextUrl: null
};

function articlesSet(previousState = articlesSetInitialState, action) {
    if(typeof previousState == 'undefined'){
        previousState = articlesSetInitialState
    }

    switch(action.type) {
        case types.ARTICLES_ARTICLES_REQUEST:
            return Object.assign({}, previousState, {
                isFetching: true,
                nextUrl: action.nextUrl
            });

        case types.ARTICLES_ARTICLES_LOADED:
            return Object.assign({}, previousState, {
                isFetching: false,
                items: previousState.items.concat(action.items),
                nextUrl: action.nextUrl
            });

        case types.ARTICLES_CATEGORIES_LOADED:
            var categories = previousState['categories'].concat(action.categories)
            return Object.assign({}, previousState, {categories: categories})

        default:
            return previousState;
    }
}

export default function articles(previousState = articlesInitialState, action) {
    var articlesSets

    switch(action.type) {
        case types.ARTICLES_ARTICLES_REQUEST:
            articlesSets = Object.assign({}, previousState.articlesSets, {[action.articlesSet]: articlesSet(previousState.articlesSets[action.articlesSet], action)})
            return Object.assign({}, previousState, {articlesSets:articlesSets})

        case types.ARTICLES_ARTICLES_LOADED:
            articlesSets = Object.assign({}, previousState.articlesSets, {[action.articlesSet]: articlesSet(previousState.articlesSets[action.articlesSet], action)})
            return Object.assign({}, previousState, {articlesSets:articlesSets}, {articles: merge({}, previousState.articles, action.articles)})

        case types.ARTICLES_CATEGORIES_LOADED:
            return Object.assign({}, previousState, {categories: action.categories})

        case types.ARTICLES_TAGS_LOADED:
            return Object.assign({}, previousState, {tags: action.tags})

        case types.ARTICLES_ARTICLE_LOADED:
            return Object.assign({}, previousState, {article: action.article})

        default:
            return previousState;
    }
}
