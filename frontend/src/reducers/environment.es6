import * as types from 'constants/ActionTypes.es6';

const initialState = {
    isMobile: null,
    device: null,
    height: null,
    width: null
};

export default function environment(state = initialState, action) {
    switch(action.type) {
        case types.ENVIRONMENT_CHANGED_SIZE:
            return Object.assign({}, state, {
                height: action.height,
                width: action.width,
            });

        case types.ENVIRONMENT_DEVICE_CHANGED:
            return Object.assign({}, state, {
                isMobile: action.device == 'mobile',
                device: action.device
            });

        default:
            return state;
    }
}