import * as types from '../constants/ActionTypes.es6';
import merge from 'lodash/object/merge';

const indexInitialState = {
    carouselImages: [],
    teasers: [],
    imageTeasers: [],
};

export default function index(previousState = indexInitialState, action) {
    switch(action.type) {
        case types.INDEX_CAROUSEL_IMAGES_LOADED:
            return Object.assign({}, previousState, {carouselImages: action.images})
        case types.INDEX_TEASERS_LOADED:
            return Object.assign({}, previousState, {teasers: action.teasers})
        case types.INDEX_IMAGE_TEASERS_LOADED:
            return Object.assign({}, previousState, {imageTeasers: action.imageTeasers})

        default:
            return previousState;
    }
}
