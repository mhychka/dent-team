import {combineReducers} from 'redux';
import environment from 'reducers/environment.es6';
import articles from 'reducers/articles.es6';
import texts from 'reducers/texts.es6';
import site from 'reducers/site.es6';
import index from 'reducers/index.es6';
import {routerReducer} from 'react-router-redux'
var {reducer: formReducer} = require('redux-form')
import { reducer as notifications } from 'react-redux-notifications'


const rootReducer = combineReducers({
    texts,
    articles,
    site,
    environment,
    index,
    form: formReducer,
    routing: routerReducer,
    notifications
});

export default rootReducer;
