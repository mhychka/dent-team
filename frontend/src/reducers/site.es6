import * as types from 'constants/ActionTypes.es6';

const initialState = {
    title:null,
    language:null
};

export default function site(state = initialState, action) {
    switch(action.type) {
        case types.SITE_TITLE_CHANGED:
            return Object.assign({}, state, {
                title: action.title
            });

        case types.SITE_LANGUAGE:
            return Object.assign({}, state, {
                language: action.lang
            });

        default:
            return state;
    }
}
