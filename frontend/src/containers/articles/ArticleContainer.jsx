import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Article from 'components/articles/Article.jsx'
import {ArticlesUrls, MainUrls} from 'constants/Urls.es6'
import {fetchArticleIfNeed} from 'actions/articles.es6'
import {Link} from 'react-router'
import {fetchCategoriesIfNeed} from 'actions/articles.es6'
import Loading from 'components/Loading.jsx'


class ArticleContainer extends Component {
    componentWillMount(){
        const {dispatch, alias, articles} = this.props
        dispatch(fetchArticleIfNeed(alias))
    }

    componentWillReceiveProps(nextProps){
        var {dispatch, text, alias} = nextProps

        if(this.props.alias != alias){
            dispatch(fetchArticleIfNeed(alias))
        }
    }

    render() {
        const {dispatch, params, article} = this.props
        if(!article){
            return <Loading />
        }

        return (
            <div>
                <section id="inner-headline" className="hidden-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <ul className="breadcrumb">
                                    <li><Link to={MainUrls.index}><i className="fa fa-home"> </i></Link><i className="icon-angle-right"> </i> </li>
                                    <li><Link to={MainUrls.text('services')}>{'Послуги'}</Link></li>
                                    <li><i className="icon-angle-right"> </i>{article.title}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content" className="articles">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-12 col-sm-12">
                                <Article key={`article-${article.id}`} article={article} />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const {environment, articles} = state;

    return {
        environment,
        articles,
        alias: props.params.alias,
        article: articles.article && articles.article.alias == props.params.alias ? articles.article : null
    };
}

export default connect(mapStateToProps)(ArticleContainer);