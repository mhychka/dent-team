import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import ArticlesList from 'components/articles/ArticlesList.jsx'
import ArticlesLayout from 'layouts/ArticlesLayout.jsx'
import {changeTitle} from 'actions/site.es6'
import {queryString} from 'utils/url.es6'
import {fetchArticlesIfNeed} from 'actions/articles.es6'
import {fetchCategoriesIfNeed, fetchTagsIfNeed} from 'actions/articles.es6'
import ArticlesCategories from 'components/articles/ArticlesCategories.jsx'
import ArticlesTags from 'components/articles/ArticlesTags.jsx'
import ArticlesPopular from 'components/articles/ArticlesPopular.jsx'


class ArticlesListContainer extends Component {
    componentWillMount(){
        const {dispatch, articlesSetId, articlesSetPopularId, articlesSets, categories, tags} = this.props

        if (!(articlesSetId in articlesSets) || !articlesSets[articlesSetId].items.length) {
            dispatch(fetchArticlesIfNeed(articlesSetId));
        }

        if (!(articlesSetPopularId in articlesSets) || !articlesSets[articlesSetPopularId].items.length) {
            dispatch(fetchArticlesIfNeed(articlesSetPopularId));
        }

        if(!categories.length){
            dispatch(fetchCategoriesIfNeed())
        }

        if(!tags.length){
            dispatch(fetchTagsIfNeed())
        }
    }

    componentWillReceiveProps(nextProps){
        const {dispatch, articlesSetId, articlesSets, categories} = this.props

        if (!(nextProps.articlesSetId in nextProps.articlesSets) || !nextProps.articlesSets[nextProps.articlesSetId].items.length) {
            dispatch(fetchArticlesIfNeed(nextProps.articlesSetId));
        }
    }

    componentDidMount(){
        const {dispatch} = this.props;
        dispatch(changeTitle(gettext('Articles')))
    }

    renderRightColumn(){
        const {environment, categories, tags, articlesSetPopularId, articlesSets} = this.props;
        if(environment.isMobile){
            return
        }

        var articlesSetPopular = articlesSetPopularId in articlesSets ? articlesSets[articlesSetPopularId] : {}

        return (
            <div className="col-lg-4 col-md-4 col-sm-3">
                <aside className="right-sidebar">
                    <ArticlesCategories categories={categories} />
                    <ArticlesTags tags={tags} />
                    <ArticlesPopular articlesSet={articlesSetPopular}/>
                </aside>
            </div>
        )
    }

    renderArticlesList(){
        const {articlesSets, articlesSetId, articles} = this.props
        var articlesSet = articlesSets[articlesSetId]
        if(articlesSet){
            return <ArticlesList articlesSet={articlesSet} articlesSetId={articlesSetId} articles={articles} />
        }
        return ''
    }

    render() {
        const {params} = this.props

        return (
            <ArticlesLayout>
                <div className="row">
                    <div className="col-lg-8 col-md-8 col-sm-9">
                        {this.renderArticlesList()}
                    </div>
                    {this.renderRightColumn()}
                </div>
            </ArticlesLayout>
        );
    }
}


function mapStateToProps(state, props) {
    const {environment, articles} = state;

    return {
        environment,
        articlesSets: articles.articlesSets,
        articlesSetId: queryString(props.params),
        articlesSetPopularId: '?aliases=article-99,article-94',
        articles: articles.articles,
        categories: articles.categories,
        tags: articles.tags,
    };
}


export default connect(mapStateToProps)(ArticlesListContainer);
