import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {fetchTextIfNeed} from 'actions/texts.es6'
import {MainUrls} from 'constants/Urls.es6';
import {Link} from 'react-router'
import Text from 'components/texts/Text.jsx'
import Loading from 'components/Loading.jsx'


class TextContainer extends Component {
    componentWillMount(){
        const {dispatch, alias} = this.props
        dispatch(fetchTextIfNeed(alias))
    }

    componentWillReceiveProps(nextProps){
        var {dispatch, text, alias} = nextProps

        if(this.props.alias != alias){
            dispatch(fetchTextIfNeed(alias))
        }
    }

    render() {
        const {alias, text} = this.props;
        if(!text){
            return <Loading />
        }

        return (
            <div>
                <section id="inner-headline" className="hidden-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <ul className="breadcrumb">
                                    <li><Link to={MainUrls.index}><i className="fa fa-home"> </i></Link><i className="icon-angle-right"> </i> </li>
                                    <li><a href="javascript://">{text.title}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content" className="articles">
                    <div className="container html-text">
                        <div className="row">
                            <div className="col-lg-9 col-md-12 col-sm-12">
                                <Text key={`text-${text.id}`} text={text}/>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const {environment, site, index, texts} = state;
    return {
        environment,
        site,
        alias: props.params.alias,
        texts: texts.texts,
        text: texts.texts ? texts.texts[props.params.alias] : null,
    };
}

export default connect(mapStateToProps)(TextContainer);