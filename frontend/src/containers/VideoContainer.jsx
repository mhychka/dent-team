import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import ThemeLayout from 'layouts/ThemeLayout.jsx'
import {changeTitle, scrollToTop} from 'actions/site.es6'
import {MainUrls} from 'constants/Urls.es6';
import {Link} from 'react-router'
import Loading from 'components/Loading.jsx'

import YouTube from 'react-youtube'

class VideoContainer extends Component {
    componentDidMount(){
        const {dispatch} = this.props
        dispatch(changeTitle(gettext('Video')))
        scrollToTop()
    }

    render() {
        var playerOpts = {width:853, height:480, playerVars: {showinfo:0}}
        return (
            <ThemeLayout>
                <div>
                    <section id="inner-headline" className="hidden-xs">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <ul className="breadcrumb">
                                        <li><Link to={MainUrls.index}><i className="fa fa-home"> </i></Link><i className="icon-angle-right"> </i> </li>
                                        <li><a href="javascript://">{gettext('Video')}</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="content" className="articles">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-9 col-md-12 col-sm-12">
                                    <div className="videobox">
                                        <h4>{gettext('Logistik im Klinikum Ernst von Bergmann')}</h4>
                                        <div className="videowrapper">
                                             <YouTube videoId="sE5cYRwS3cU" opts={playerOpts}  />
                                        </div>
                                    </div>
                                    <div className="videobox">
                                        <h4>{gettext('Klinik für Kinderchirurgie - Unfallprävention für Kinder und Jugendliche')}</h4>
                                        <div className="videowrapper">
                                             <YouTube videoId="jeagj5RLhzU" opts={playerOpts}  />
                                        </div>
                                    </div>
                                    <div className="videobox">
                                        <h4>{gettext('Natürlich in Sicherheit - Die Geburt in Potsdam')}</h4>
                                        <div className="videowrapper">
                                             <YouTube videoId="2sjLGv-NL2Y" opts={playerOpts}  />
                                        </div>
                                    </div>
                                    <div className="videobox">
                                        <h4>{gettext('Herzkissen - Von Frauen für Frauen')}</h4>
                                        <div className="videowrapper">
                                             <YouTube videoId="c1SLUBHSZik" opts={playerOpts}  />
                                        </div>
                                    </div>

                                    <div className="videobox">
                                        <h4>{gettext('Die Schilddrüsen-Ambulanz am MVZ Ernst von Bergmann Potsdam')}</h4>
                                        <div className="videowrapper">
                                             <YouTube videoId="uzK5OBNFkSE" opts={playerOpts}  />
                                        </div>
                                    </div>
                                    <div className="videobox">
                                        <h4>{gettext('Das SPECT CT - neues Diagnoseverfahren in der Nuklearmedizin')}</h4>
                                        <div className="videowrapper">
                                             <YouTube videoId="tBHk9Ocr1ME" opts={playerOpts}  />
                                        </div>
                                    </div>
                                    <br /><br /> <a href="https://www.youtube.com/channel/UCCwFgtvLM6LF3qyvMyTDEEQ" target="_blank" className="btn btn-theme">{gettext('More Video')}</a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </ThemeLayout>
        );
    }
}

function mapStateToProps(state, props) {
    const {environment, site} = state;
    return {
        environment,
        site,
    };
}

export default connect(mapStateToProps)(VideoContainer);