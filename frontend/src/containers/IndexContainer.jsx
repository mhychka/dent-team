import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Flexslider from 'components/Flexslider.jsx'
import Teasers from 'components/Teasers.jsx'
import ImageTeasers from 'components/ImageTeasers.jsx'
import {changeTitle, scrollToTop} from 'actions/site.es6'
import {fetchCarouselImages, fetchTeasers, fetchImageTeasers} from 'actions/index.es6'
import Loading from 'components/Loading.jsx'


class IndexContainer extends Component {
    componentDidMount(){
        const {dispatch} = this.props;
        dispatch(changeTitle(gettext('Home')))
    }

    componentWillMount() {
        const {dispatch, images, teasers, imageTeasers} = this.props;
        if (!images.length) {
            dispatch(fetchCarouselImages());
        }
        if (!teasers.length) {
            dispatch(fetchTeasers());
        }
        if (!imageTeasers.length) {
            dispatch(fetchImageTeasers());
        }
    }

    render() {
        const {images, teasers, imageTeasers} = this.props;
        if(!images || !teasers || !imageTeasers){
            return <Loading />
        }

        return (
            <div>
                <section id="featured">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <Flexslider images={images}/>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="callaction">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="big-cta">
                                    <div className="cta-text">
                                        <h2>
                                            <span>Dent Team</span>
                                        </h2>
                                        {'Cучасна естетична стоматологiя в м.Умань'}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="content">
                    <div className="container">
                        <div className="row" style={{marginBottom: '0px'}}>
                            <div className="col-lg-12">
                                <Teasers teasers={teasers} />
                            </div>
                        </div>
                        <div className="row" style={{marginBottom: '0px'}}>
                            <div className="col-lg-12">
                                <div className="solidline"></div>
                            </div>
                        </div>
                        <div className="row" >
                            <div className="col-lg-12">
                                <ImageTeasers images={imageTeasers} cId="index" title={gettext('Interior')} />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const {environment, site, index} = state;
    return {
        environment,
        site,
        images: index.carouselImages,
        teasers: index.teasers,
        imageTeasers: index.imageTeasers
    };
}

export default connect(mapStateToProps)(IndexContainer);
