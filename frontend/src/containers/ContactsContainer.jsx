import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {changeTitle, scrollToTop} from 'actions/site.es6'
import {fetchTextIfNeed} from 'actions/texts.es6'
import Loading from 'components/Loading.jsx'
import Contacts from 'components/Contacts.jsx'
import {Link} from 'react-router'
import {MainUrls} from 'constants/Urls.es6';
import ImageTeasers from 'components/ImageTeasers.jsx'
import ContactMessageForm from 'components/ContactMessageForm.jsx'
import * as config from 'constants/Config.es6';


class ContactsContainer extends Component {
    componentWillMount(){
        const {dispatch, texts} = this.props
        dispatch(fetchTextIfNeed(`contacts-images`))
    }

    componentDidMount(){
        const {dispatch} = this.props;
        dispatch(changeTitle('Контакти'))
    }

    render() {
        const {dispatch, texts} = this.props
        if(!texts[`contacts-images`]){
            return <Loading />
        }

        return (
            <div>
                <section id="inner-headline" className="hidden-xs">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <ul className="breadcrumb">
                                    <li><Link to={MainUrls.index}><i className="fa fa-home"> </i></Link><i className="icon-angle-right"> </i> </li>
                                    <li><a href="javascript://">{gettext('Contacts')}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content" className="articles">
                    <div className="container">
                        <h3>Контакти</h3>
                        <Contacts />
                        <ContactMessageForm />
                        <div className="contacts-images">
                            <ImageTeasers images={texts[`contacts-images`].images} cId="contacts" title={'Наша команда'} />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    const {environment, site, texts} = state;
    return {
        environment,
        site,
        texts: texts.texts
    };
}

export default connect(mapStateToProps)(ContactsContainer);
