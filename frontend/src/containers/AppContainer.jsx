import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {initEnvironment} from 'actions/environment.es6';
import {initSite} from 'actions/site.es6';
import {scrollToTop} from 'actions/site.es6'
import * as config from 'constants/Config.es6';
import SidebarMobile from 'components/SidebarMobile.jsx'
import HeaderMobile from 'components/HeaderMobile.jsx'
import HeaderDesctop from 'components/HeaderDesctop.jsx'
import Footer from 'components/Footer.jsx'
import SocialButtons from 'components/SocialButtons.jsx'


class AppContainer extends Component {
    componentDidMount() {
        const {dispatch, environment} = this.props;
        dispatch(initEnvironment());
        dispatch(initSite());
    }

    renderSidebar(){
        const {isMobile} = this.props
        if(isMobile){
            return <SidebarMobile />
        }
    }

    renderHeader(){
        const {isMobile} = this.props
        if(isMobile){
            return <HeaderMobile />
        }
        return <HeaderDesctop />
    }

    renderSocialButtons(){
        const {isMobile} = this.props
        if(!isMobile){
            return <SocialButtons />
        }
    }

    render() {
        const {children, environment} = this.props

        return(
            <div className={`main-container ${environment.device}`}>
                {this.renderSidebar()}
                <div className="main-content-box">
                    {this.renderHeader()}
                    <div className="main-content" id="panel">
                        {children}
                        <Footer />
                        {this.renderSocialButtons()}
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {environment} = state;
    return {
        environment,
        isMobile: environment.isMobile
    };
}

export default connect(mapStateToProps)(AppContainer);