require('script!static/js/jquery/jquery.js')
require('script!static/bootstrap/js/bootstrap.min.js')
require('script!static/js/jquery/plugins/jquery.easing.1.3.js')
require('script!static/js/jquery/plugins/fancybox/jquery.fancybox.pack.js')
require('script!static/js/jquery/plugins/fancybox/helpers/jquery.fancybox-media.js')
require('script!static/js/jquery/plugins/fancybox/helpers/jquery.fancybox-buttons.js')
require('script!static/js/jquery/plugins/flexslider/jquery.flexslider.js')
require('script!static/js/animate.js')
require('script!slideout/dist/slideout.min.js')
import * as config from 'constants/Config.es6';


import 'babel-core/polyfill'
import 'fastclick'
import fetch from 'isomorphic-fetch'
import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import configureStore from 'store/configureStore.es6'
import routes from 'routes.jsx'
import {Router, useRouterHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import {browserHistory} from 'react-router'
import useScroll from 'scroll-behavior/lib/useStandardScroll'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import {initGoogleAnalytics} from 'utils/statistic.es6'

if(NODE_ENV == 'production'){
    initGoogleAnalytics(config.GOOGLEANALYTICS_KEY)
}
const createScrollHistory = useScroll(createBrowserHistory)
window.appHistory = useRouterHistory(createScrollHistory)()

const store = configureStore();
const history = syncHistoryWithStore(appHistory, store)

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            {routes}
        </Router>
    </Provider>,
    document.getElementById('wrapper')
);