export function makeQueryString(object) {
    var ret = [];
    for (var d in object) {
        ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(object[d]))
    }
    return ret.join("&")
}

export function queryString(params) {
    return `?${makeQueryString(params)}`
}

export function pathMatch(path, pattern, strict = false) {
    if(strict){
        return path == pattern
    }
    return path.indexOf(pattern) != -1 || path.indexOf((pattern+'/')) != -1
}

export function activeClass(path, pattern, className='active', strict = false){
    return pathMatch(path, pattern, strict) ? className : ''
}