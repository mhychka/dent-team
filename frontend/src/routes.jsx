import React from 'react'
import ArticleContainer from 'containers/articles/ArticleContainer.jsx'
import AppContainer from 'containers/AppContainer.jsx'
import IndexContainer from 'containers/IndexContainer.jsx'
import TextContainer from 'containers/texts/TextContainer.jsx'
import ContactsContainer from 'containers/ContactsContainer.jsx'
import {Route, IndexRoute} from 'react-router'


export default (
    <Route path="/app" component={AppContainer} >
        <IndexRoute component={IndexContainer} />
        <Route path="articles">
            <Route path=":alias" component={ArticleContainer} />
        </Route>
        <Route path="text/:alias" component={TextContainer} />
        <Route path="contacts" component={ContactsContainer} />
    </Route>
);