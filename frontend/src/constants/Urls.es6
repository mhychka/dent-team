import {isActive} from 'react-router'

export var AuthUrls = {
    login: '/api/v1/user/login/'
}
var urlPrefix = '/app'

export var MainUrls = {
    index:`${urlPrefix}/`,
    text: (alias) => `${urlPrefix}/text/${alias}`,
    contacts: `${urlPrefix}/contacts`,
    video: `${urlPrefix}/video`,
}

export var I18nUrls = {
    api:{
        language: '/api/v1/i18n/language'
    }
}

export var ContactsUrls = {
    api:{
        message: '/api/v1/contacts/contactmessage'
    }
}

export var ArticlesUrls = {
    articles:`${urlPrefix}/articles/`,
    articlesCategory:(alias) => `${urlPrefix}/articles/category/${alias}`,
    articlesTag:(alias) => `${urlPrefix}/articles/tag/${alias}`,
    article:(alias) => `${urlPrefix}/articles/${alias}`,
    api: {
        articles: '/api/v1/articles/articles',
        article: (alias) => `/api/v1/articles/article/${alias}`,
        categories: '/api/v1/articles/categories',
        tags: '/api/v1/articles/tags'
    }
}

export var TextsUrls = {
    api: {
        texts: '/api/v1/texts/texts',
        text: (alias) => `/api/v1/texts/text/${alias}`
    }
}