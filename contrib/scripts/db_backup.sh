#!/bin/bash

cd `dirname $0`
pwd
backup_path="/home/mgi4ka/backup/db"
year=$(date +%Y)
month=$(date +%m)
time_stamp=$(date +%Y-%m-%d-%T)
mkdir -p "${backup_path}/${year}/${month}"
pg_dump -U postgres evb_db > "${backup_path}/${year}/${month}/evb_db_${time_stamp}.sql"