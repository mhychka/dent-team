#!/bin/bash

cd /var/www/dent-team
echo "update translations..."
python3 backend/manage.py makemessages -l uk --ignore vendors
python3 backend/manage.py makemessages -d djangojs -l uk --ignore node_modules --extension js,es6,jsx
python3 backend/manage.py compilemessages
python3 backend/manage.py compilejsi18n
echo "DONE!"