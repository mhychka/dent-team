#!/bin/bash

echo "WEBSERVER START!"

cd /var/www/dent-team
pwd

VERSION=`date +%d%m%Y%H%M%S`
echo "version ...$VERSION"
echo $VERSION > build.txt

echo "git status ..."
git status
echo "git pull ..."
git pull

echo "requirements installing ..."
sudo pip3 install -r backend/requirements.txt

echo "sync db ..."
python3 backend/manage.py syncdb

echo "update translations..."
python3 backend/manage.py compilemessages
python3 backend/manage.py compilejsi18n

echo "static files ..."
rm -Rf static/css/compressed/*
python3 backend/manage.py collectstatic --noinput --settings main.settings.pipeline
cd frontend
npm install
rm -Rf static/js/compressed/*
rm -Rf static/js/bundles/*
NODE_ENV=production node_modules/.bin/webpack

cd /var/www/dent-team
pwd

#uwsgi
uwsgi --ini /var/www/dent-team/contrib/conf/webserver/uwsgi.ini

#supervisord
service supervisor restart
supervisorctl restart all

#sitemap
python3 /var/www/dent-team/backend/manage.py refresh_sitemap

echo "git status ..."
git status
