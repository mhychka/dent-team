#!/bin/bash

echo "WEBSERVER START!"

cd /var/www/dent-team
pwd

VERSION=`date +%d%m%Y%H%M%S`
echo "version ...$VERSION"
echo $VERSION > build.txt

echo "git status ..."
git status
echo "git pull ..."
git pull

echo "requirements installing ..."
sudo pip3 install -r backend/requirements.txt

echo "sync db ..."
python3 backend/manage.py syncdb

cd /var/www/dent-team
pwd

#uwsgi
uwsgi --ini contrib/conf/webserver/uwsgi.ini

#supervisord
service supervisor restart
supervisorctl restart all

#sitemap
python3 backend/manage.py refresh_sitemap

echo "git status ..."
git status
