from django.utils.translation import ugettext_lazy as _

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard


class AdminIndexDashboard(Dashboard):
    def init_with_context(self, context):
        # append an app list module for "Applications"
        self.children.append(
            modules.Group(
                title='Applications',
                display="tabs",
                children=[
                    modules.AppList(
                        title='Users',
                        models=('django.contrib.auth.*','authentication.models.AuthUser')
                    ),

                    # modules.AppList(
                    # title=u'Вторичная недвижимость',
                    # models=('resell.*',)
                    # ),
                    # modules.AppList(
                    # title=u'Тендеры',
                    # models=('tender.*',)
                    # ),
                    # modules.AppList(
                    #     title=u'Проекты Формулы Ремонта',
                    #     models=('project.*',)
                    # ),
                ]
            ),
        )

        # # append an app list module for "Administration"
        # self.children.append(modules.Group(
        # title=u"Администрирование",
        # display="tabs",
        # children=[
        # modules.AppList(
        #             title=u'Пользователи',
        #             models=('django.contrib.auth.*',)
        #         ),
        #
        #         modules.AppList(
        #             title=u'Меню',
        #             models=('cms.*',)
        #         ),
        #     ]
        # ))
        #
        # # append a recent actions module
        # #self.children.append(modules.RecentActions(_('Recent Actions'), 5))
        #
        # # append another link list module for "support".
        # self.children.append(modules.LinkList(
        #     _('Support'),
        #     children=[
        #         {
        #             'title': u'Сайт Signception',
        #             'url': 'http://signception.ru/',
        #             'external': True,
        #         },
        #         {
        #             'title': u'Багрепорт',
        #             'url': 'http://ticket.signception.ru/',
        #             'external': True,
        #         },
        #         {
        #             'title': u'Телефон: +7 (904) 38-159-63',
        #             'url': '#',
        #             'external': False,
        #         },
        #     ]
        # ))


class CustomAppIndexDashboard(AppIndexDashboard):
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(_(self.app_title), self.models),
            modules.RecentActions(
                'Recent Actions',
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
