from rest_framework import permissions

class IsOwner(permissions.BasePermission):
    def __init__(self, attr_name = 'owner_id'):
        self.attr_name = attr_name

    def has_object_permission(self, request, view, obj):
        value = getattr(obj, self.attr_name)
        return request.user.is_authenticated() and value == request.user.id