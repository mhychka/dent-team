from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from api.serializers.texts.textimage import TextImageSerializer
from texts.models import Text
import json

AuthUser = get_user_model()


class TextImagesView(viewsets.ModelViewSet):
    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super().get_permissions()

    def create(self, request, *args, **kwargs):
        params = dict((key, value) for (key, value) in request.data.items())
        file = request.FILES.getlist('file')[0]

        data = dict(text=params['parent_id'], file=file, owner=self.request.user.pk)
        serializer = TextImageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)