from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from api.permissions import *
from texts.models import Text
import json
AuthUser = get_user_model()


class TextsView(viewsets.ModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        params = dict((key, value) for (key, value) in request.GET.items())
        fields = params.pop('fields', '').split(',')
        aliases = params.pop('aliases', '').split(',')

        texts = Text.objects.filter(alias__in=aliases).order_by('-priority', 'created')
        data = [text.get_data(fields, request.LANGUAGE_CODE) for text in texts]
        return Response(data, status=status.HTTP_200_OK)


class TextView(viewsets.ModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        from texts.models import Text
        params = dict((key, value) for (key, value) in request.GET.items())
        article = Text.objects.get(alias=self.kwargs['alias'])
        return Response(article.get_data('all', request.LANGUAGE_CODE), status=status.HTTP_200_OK)