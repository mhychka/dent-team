from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from api.permissions import *
import json
from api.serializers.contacts.contactmessage import ContactMessageSerializer
AuthUser = get_user_model()


class ContactMessageView(viewsets.ModelViewSet):
    def create(self, request, *args, **kwargs):
        data = dict((key, value) for (key, value) in request.DATA.items())

        serializer = ContactMessageSerializer(request=request, data=data)
        if serializer.is_valid():
            obj = serializer.save()
            return Response(obj.data(), status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)