from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
import json
from django.conf import settings
AuthUser = get_user_model()


class I18nLanguageView(viewsets.ModelViewSet):

    def update(self, request, *args, **kwargs):
        params = dict((key, value) for (key, value) in request.DATA.items())
        lang = params['lang']

        response = Response({'lang': lang}, status=status.HTTP_200_OK)
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang, 366 * 24 * 60 * 60 )
        return response