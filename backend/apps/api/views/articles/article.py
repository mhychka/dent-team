from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from api.serializers.texts.textimage import TextImageSerializer
import json
from urllib.parse import urlencode
from django.core.urlresolvers import reverse
from api.serializers.articles.articleimage import ArticleImageSerializer
AuthUser = get_user_model()


class ArticlesView(viewsets.ModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        from articles.services.article import ArticlesListService
        params = dict((key, value) for (key, value) in request.GET.items())

        service = ArticlesListService(request.user)
        data, next_page_number = service.get_data(params)
        next_url = None
        if next_page_number:
            get = dict(request.GET)
            get['page'] = next_page_number
            next_url = reverse('api:articles_articles') + '?{params}'.format(params=urlencode(get, doseq=True))
        data = {'keys':{'nextUrl': next_url}, 'data': [item.get_data(['images', 'preview_formatted', 'category'], lang=request.LANGUAGE_CODE) for item in data]}
        return Response(data, status=status.HTTP_200_OK)


class ArticleView(viewsets.ModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        from articles.models import Article
        params = dict((key, value) for (key, value) in request.GET.items())
        article = Article.objects.get(alias=self.kwargs['alias'])
        return Response(article.get_data('all', request.LANGUAGE_CODE), status=status.HTTP_200_OK)


class ArticleImagesView(viewsets.ModelViewSet):
    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super().get_permissions()

    def create(self, request, *args, **kwargs):
        params = dict((key, value) for (key, value) in request.data.items())
        file = request.FILES.getlist('file')[0]

        data = dict(article=params['parent_id'], file=file, owner=self.request.user.pk)
        serializer = ArticleImageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)