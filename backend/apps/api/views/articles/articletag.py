from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
import json
from urllib.parse import urlencode
from django.core.urlresolvers import reverse
AuthUser = get_user_model()
from articles.models import ArticleTag


class ArticleTagsView(viewsets.ModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        params = dict((key, value) for (key, value) in request.GET.items())

        tags = ArticleTag.objects.all()
        data = [t.data(lang=request.LANGUAGE_CODE) for t in tags]
        return Response(data, status=status.HTTP_200_OK)