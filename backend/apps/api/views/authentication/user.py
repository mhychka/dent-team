from rest_framework import views, viewsets, serializers
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from authentication.models import AuthUser
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist


class AuthUsersView(viewsets.ModelViewSet):
    def get_permissions(self):
        return (IsAuthenticated(),)

    def get_user(self):
        user = self.request.user
        self.check_object_permissions(self.request, user)
        return user

    def retrieve(self, request, *args, **kwargs):
        from authentication.services.user import AuthUserListService
        params = dict((key, value) for (key, value) in request.GET.items())
        datatype = params.pop('datatype', None)
        user = self.get_user()

        service = AuthUserListService(request.user)
        users = service.get_data(params)
        data = {}

        #autocomplete
        if datatype == 'ac':
            data = {'suggestions': []}
            for user in users:
                if type(user) is not dict:
                    user = user.data
                data['suggestions'].append({'data': user, 'value': '{name}({email})'.format(name=user['name'], email=user['email'])})

        return Response(data, status=status.HTTP_200_OK)


class AuthUserSessionView(viewsets.ModelViewSet):
    def retrieve(self, request, *args, **kwargs):
        from redis_sessions.session import SessionStore

        sessid = request.GET.get('sessid', None)
        if sessid:
            data = {'success': False, 'msg':'Session is not valid'}
            store = SessionStore(sessid)
            if store.exists(sessid):
                user_data = store.load()
                id = user_data['_auth_user_id']
                try:
                    user = AuthUser.objects.get(pk=id)
                    data = {'success': True, 'user':{'id' : user.guid, 'name': user.name}}
                except ObjectDoesNotExist:
                    pass

            return Response(data, status=status.HTTP_200_OK)

        return Response(None, status=status.HTTP_400_BAD_REQUEST)


class AuthUserDataView(viewsets.ModelViewSet):
    def get_user(self):
        user = self.request.user if self.request.user.is_authenticated() else None
        return user

    def retrieve(self, request, *args, **kwargs):
        from authentication.services.user import AuthUserDataService
        params = dict((key, value) for (key, value) in request.GET.items())
        user = self.get_user()
        service = AuthUserDataService(user)
        data = service.get_data(['chat_new_messages_count', 'projecttasks_new_count', 'projectstasks', 'projectuserlogs_pending'], params)
        return Response(data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        from api.serializers.authentication.user import UserDataSerializer
        data = dict((key, value) for (key, value) in request.data.items())
        user = self.get_user()
        userdata = user.get_userdata()

        serializer = UserDataSerializer(instance=userdata, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = [IsAuthenticated]
        return super().get_permissions()