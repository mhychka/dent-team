from django.contrib.auth import get_user_model, authenticate
from rest_framework import views, viewsets, serializers
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
AuthUser = get_user_model()


class AuthUserProfileView(viewsets.ModelViewSet):
    def get_user(self):
        user = self.request.user if self.request.user.is_authenticated() else None
        return user

    def validate(self, request, *args, **kwargs):
        data = dict((key, value) for (key, value) in request.GET.items())
        user = self.get_user()

        if user and 'current_password' in data and not user.check_password(data['current_password']):
                return Response('Current Password is wrong',status=status.HTTP_400_BAD_REQUEST)

        serializer = AuthUserProfileSerializer(data=data, instance=user)
        if serializer.is_valid():
            return Response(status=status.HTTP_200_OK)

        attrs = data.keys()
        errors = serializer.errors
        for attr in attrs:
            if attr in errors:
                return Response(errors[attr][0], status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

    def get_permissions(self):
        if self.request.method == 'PUT':
            self.permission_classes = [IsAuthenticated]
        return super().get_permissions()

    def update(self, request, *args, **kwargs):
        data = dict((key, value) for (key, value) in request.data.items())
        user = self.get_user()
        serializer = AuthUserProfileSerializer(data=data, instance=user)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        from django.contrib.auth import authenticate, login

        data = dict((key, value) for (key, value) in request.data.items())

        serializer = AuthUserProfileSerializer(data=data)
        if serializer.is_valid():
            user = serializer.save()
            user = authenticate(username=user.username, password=str(data['new_password']))
            login(self.request, user)
            return Response(user.get_data(['company',]), status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuthUserProfileImageView(viewsets.ModelViewSet):
    def get_user(self):
        user = self.request.user
        return user

    def get_permissions(self):
        if self.request.method == 'POST':
            self.permission_classes = [IsAuthenticated]
        return super().get_permissions()

    def create(self, request, *args, **kwargs):
        user = self.get_user()

        file = request.FILES.getlist('files')[0]
        if not file:
            return Response({'file': ['Uploaded file data is missing']}, status=status.HTTP_400_BAD_REQUEST)

        data = dict(image=file)
        serializer = AuthUserProfileImageSerializer(instance=user, data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)