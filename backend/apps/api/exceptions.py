from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import APIException
from rest_framework import status


class AuthenticationFailed(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('Incorrect authentication credentials.')


class UserNotFound(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _('User not found.')


class UserExists(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = _('Such user already exists.')