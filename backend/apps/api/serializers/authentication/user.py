from django.contrib.auth import get_user_model
from rest_framework import serializers, validators
from authentication.models import AuthUserData
from main.events import create_event
from utils.string import guid
from projects.models import Project, ProjectFile
import os
import re
from datetime import datetime
from unidecode import unidecode
import ntpath

AuthUser = get_user_model()


class UserListSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        return instance.data


class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = AuthUser
        fields = ('id',)

    def to_representation(self, instance):
        return instance.data


class UserDataSerializer(serializers.ModelSerializer):
    jssid = serializers.CharField(required=False, max_length=255, allow_blank=True)

    class Meta:
        model = AuthUserData
        fields = ('chat_new_direct_messages', 'chat_new_channel_messages', 'chat_new_channel_mentions','jssid')

    def update(self, instance, validated_data):

        for field, value in validated_data.items():
            setattr(instance, field, value)
        instance.save()

        create_event('auth:authuserdata_updated', {
            'user': instance.user_id,
            'fields': ['chat_new_messages_count',],
            'params': dict(),
            'evsid': validated_data['jssid']
        })

        return instance

    def to_representation(self, instance):
        return instance.data