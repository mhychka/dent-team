from django.contrib.auth import get_user_model
from rest_framework import serializers, validators
import os
from api.serializers import fields
from authentication.models import AuthUser
from django.core.exceptions import ObjectDoesNotExist
import datetime
from django.utils import timezone


class AuthUserProfileSerializer(serializers.ModelSerializer):
    jssid = serializers.CharField(required=False, max_length=255, allow_blank=True)
    email = serializers.EmailField(required=True, max_length=100,
                validators=[validators.UniqueValidator(queryset=AuthUser.objects.all(), message="Email already in use")])
    username = serializers.RegexField(regex=r'^[0-9a-z-._]+$', required=True, max_length=50,
                validators=[validators.UniqueValidator(queryset=AuthUser.objects.all(), message="Username already in use")])
    language = serializers.CharField(required=False, max_length=4)
    current_password = serializers.CharField(required=False, min_length=4, max_length=30, allow_blank=True)
    new_password = serializers.CharField(required=False, min_length=4, max_length=30, allow_blank=True)

    class Meta:
        fields = ('first_name', 'username','email', 'phone', 'email', 'jssid', 'current_password', 'new_password', 'language')
        model = AuthUser

    def update(self, instance, validated_data):
        current_password = validated_data.pop('current_password', None)
        new_password = validated_data.pop('new_password', None)

        if 'email' in validated_data:
            validated_data['email'] = validated_data['email'].strip().lower()
        if 'username' in validated_data:
            validated_data['username'] = validated_data['username'].strip().lower()
        if 'language' in validated_data:
            validated_data['language'] = validated_data['language'].strip().lower()

        if new_password and not current_password:
            raise serializers.ValidationError({'current_password': ['Need to enter current password']})

        if current_password:
            if not new_password:
                raise serializers.ValidationError({'new_password': ['Need to enter new password']})

            if not instance.check_password(current_password):
                raise serializers.ValidationError({'current_password': ['Wrong current password']})

            instance.set_password(new_password)

        for field, value in validated_data.items():
            setattr(instance, field, value)

        instance.save()

        return instance

    def create(self, validated_data):
        password = validated_data.pop('new_password', None)
        jssid = validated_data.pop('jssid', None)

        if not password:
            raise serializers.ValidationError({'new_password': ['Empty password value']})

        validated_data['email'] = validated_data['email'].strip().lower()
        validated_data['username'] = validated_data['username'].strip().lower()
        validated_data['last_login'] = datetime.datetime.now(timezone.utc)
        validated_data['guid'] = AuthUser.get_guid()
        user = AuthUser.objects.create(**validated_data)
        user.set_password(password)
        user.save()

        return user

    def to_representation(self, instance):
        return instance.data


class AuthUserProfileImageSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('image',)
        model = AuthUser

    def validate_image(self, value):
        pass
        file = value
        upload_max_size = AuthUser.FILE_UPLOAD_MAX_SIZE * 1024 * 1024
        allowed_types = AuthUser.FILE_UPLOAD_ALLOWED_TYPES
        if (file.size >= upload_max_size):
            raise serializers.ValidationError({'image': ["Allowed max file size is {mb}MB".format(mb=AuthUser.FILE_UPLOAD_MAX_SIZE)]})

        extension = os.path.splitext(file.name)[1]
        extension = extension.lower()
        if extension not in allowed_types:
            raise serializers.ValidationError({'image': ["Allowed file types is {types}".format(types=', '.join(allowed_types))]})

        return value

    def update(self, instance, validated_data):
        image = validated_data['image']
        instance.image = image
        instance.save()

        return instance

    def to_representation(self, instance):
        return instance.get_data('thumbs')