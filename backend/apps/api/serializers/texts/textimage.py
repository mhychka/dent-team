from django.contrib.auth import get_user_model
from rest_framework import serializers, validators
from texts.models import Text, TextImage
import os
from rest_framework.validators import UniqueTogetherValidator
from api.serializers.fields import CodeField
from utils.image import get_image_size
import ntpath
AuthUser = get_user_model()


class TextImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = TextImage
        fields = ('owner', 'text', 'file', 'name')

    def validate_file(self, value):
        file = value
        upload_max_size = TextImage.FILE_UPLOAD_MAX_SIZE * 1024 * 1024
        allowed_types = TextImage.FILE_UPLOAD_ALLOWED_TYPES
        if (file.size >= upload_max_size):
            raise serializers.ValidationError(
                "Allowed max file size is {mb}MB".format(mb=TextImage.FILE_UPLOAD_MAX_SIZE))

        extension = os.path.splitext(file.name)[1]
        extension = extension.lower()
        if extension not in allowed_types:
            raise serializers.ValidationError(
                "Allowed file types is {types}".format(
                    types=', '.join(allowed_types)))

        return value

    def create(self, validated_data):
        file = validated_data['file']
        text = validated_data['text']
        owner = validated_data['owner']

        fields = {
            'size': file.size,
            'mime_type': file.content_type,
            'file': file,
            'source_filename': file.name,
            'guid': TextImage.get_guid(),
            'source': TextImage.SOURCE_LOCAL,
            'name': file.name,
            'owner': owner,
            'text': text,
            'status':TextImage.STATUS_READY,
        }

        obj = TextImage.objects.create(**fields)
        width, height = get_image_size(obj.file.path)
        obj.filename = ntpath.basename(obj.file.url)
        obj.width = width
        obj.height = height
        obj.save()

        return obj

    def to_representation(self, instance):
        return instance.get_data(['thumbs'])