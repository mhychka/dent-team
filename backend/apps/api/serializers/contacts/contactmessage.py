from django.contrib.auth import get_user_model
from rest_framework import serializers, validators
from contacts.models import ContactMessage
import os
from rest_framework.validators import UniqueTogetherValidator
from utils.image import get_image_size
import ntpath
AuthUser = get_user_model()


class ContactMessageSerializer(serializers.ModelSerializer):
    request = None

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    class Meta:
        model = ContactMessage
        fields = ('name', 'email', 'subject', 'message',)

    def create(self, validated_data):
        from main.tasks import ContactMessageNotificationTask

        obj = ContactMessage.objects.create(**validated_data)
        obj.save()

        task = ContactMessageNotificationTask()
        task.apply_async((obj.pk,))

        return obj

    def to_representation(self, instance):
        return instance.get_data()