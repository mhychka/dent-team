import json
from django.contrib.gis.geos import GEOSGeometry
from django.core import validators
from django.utils.encoding import smart_str
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Point, MultiPoint
from rest_framework import serializers
from authentication.models import AuthUser
from django.core.exceptions import ObjectDoesNotExist


class PointField(serializers.Field):
    type_name = 'PointField'
    type_label = 'point'

    default_error_messages = {
        'invalid': 'Location field has wrong format. Use {"latitude": 45.67294621, "longitude": 26.43156}',
    }

    def to_internal_value(self, value):
        """
        Parse json data and return a point object
        """
        if value in (None, '', [], (), {}):
            return None

        value_type = type(value)
        if value_type is str:
            try:
                value = value.replace("'", '"')
                value = json.loads(value)
            except ValueError:
                msg = self.error_messages['invalid']
                raise serializers.ValidationError(msg)

        if value and type(value) is dict:
            latitude = value.get("latitude")
            longitude = value.get("longitude")
            if latitude and longitude:
                point_object = GEOSGeometry(
                    'POINT(%(longitude)s %(latitude)s)' % {
                        "longitude": longitude,
                        "latitude": latitude,
                    })
                return point_object
        else:
            msg = self.error_messages['invalid']
            raise serializers.ValidationError(msg)

    def to_representation(self, value):
        """
        Transform POINT object to json.
        """
        if value is None:
            return value

        if isinstance(value, GEOSGeometry):
            value = {
                "latitude": smart_str(value.y),
                "longitude": smart_str(value.x)
            }
        return value


class JSONField(serializers.Field):
    default_error_messages = {
        'invalid': _(
            'JSON Field has wrong format.'),
    }

    def to_internal_value(self, value):
        if value in (None, '', [], (), {}):
            return None

        value_type = type(value)
        if value_type is str:
            return value

        if value and value_type is dict:
            return json.dumps(value)
        else:
            msg = self.error_messages['invalid']
            raise serializers.ValidationError(msg)

    def to_representation(self, value):
        return value


class JSONDataField(serializers.Field):
    default_error_messages = {
        'invalid': _(
            'JSON Field has wrong format.'),
    }

    def to_internal_value(self, value):
        if value in (None, '', [], (), {}):
            return None

        value_type = type(value)
        if value_type is str:
            try:
                value = json.loads(value)
            except ValueError:
                msg = self.error_messages['invalid']
                raise serializers.ValidationError(msg)
        return value

    def to_representation(self, value):
        return value

class GeometryField(serializers.Field):
    type_name = 'GeometryField'

    default_error_messages = {
        'invalid': _(
            'Coordinates field has wrong format.'),
    }

    def to_internal_value(self, data):
        """
        Parse json data and return a point object
        """
        if data in (None, '', [], (), {}):
            return None

        if type(data) is str:
            try:
                data = data.replace("'", '"')
                data = json.loads(data)
            except ValueError:
                msg = self.error_messages['invalid']
                raise serializers.ValidationError(msg)

        if data and type(data) is dict and 'type' in data:
            field_type = data['type']
            if field_type == 'point':
                return Point(*data['value'])
            if field_type == 'multipoint':
                return MultiPoint(*[Point(latitude, longitude) for latitude, longitude in data['value']])
        else:
            msg = self.error_messages['invalid']
            raise serializers.ValidationError(msg)


class CodeField(serializers.ChoiceField):
    def to_internal_value(self, data):
        data = super().to_internal_value(data)
        return self.choices[data]


class AuthUserRequestEmailField(serializers.EmailField):
    def to_representation(self, value):
        return value

    def to_internal_value(self, data):
        try:
            AuthUserAuthUserRequest.objects.get(email=data)
            raise serializers.ValidationError('User request exists for email {email}'.format(email=data))
        except ObjectDoesNotExist:
            pass

        try:
            user = AuthUser.objects.get(email=data)
            if not user.in_group('pworker'):
                raise serializers.ValidationError('User with email {email} has been already registered as manager'.format(email=data))

            AuthUserAuthUserLink.objects.get(user=user)
            raise serializers.ValidationError('User with email {email} has been already registered'.format(email=data))
        except ObjectDoesNotExist:
            pass

        return super().to_internal_value(data)
