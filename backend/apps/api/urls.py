from django.conf.urls import patterns, include, url
from rest_framework import routers
from api.views.texts.textimage import TextImagesView
from api.views.texts.text import TextsView, TextView
from api.views.articles.article import ArticleView, ArticlesView, ArticleImagesView
from api.views.articles.articlecategory import ArticleCategoriesView
from api.views.articles.articletag import ArticleTagsView
from api.views.i18n.language import I18nLanguageView
from api.views.contacts.contactmessage import ContactMessageView

urlpatterns = patterns('',
    #texts
    url(r'^texts/text/images$', TextImagesView.as_view(dict(post='create')), name="texts_text_images"),
    url(r'^texts/texts$', TextsView.as_view(dict(get='retrieve')), name="texts_texts"),
    url(r'^texts/text/(?P<alias>.*)$', TextView.as_view(dict(get='retrieve')), name="texts_text"),

    #i18n
    url(r'^i18n/language$', I18nLanguageView.as_view(dict(put='update')), name="i18n_language"),

    #contacts
    url(r'^contacts/contactmessage$', ContactMessageView.as_view(dict(post='create')), name="contacts_contactmessage"),

    #articles
    url(r'^articles/categories$', ArticleCategoriesView.as_view(dict(get='retrieve')), name="articles_categories"),
    url(r'^articles/tags$', ArticleTagsView.as_view(dict(get='retrieve')), name="articles_tags"),
    url(r'^articles/articles$', ArticlesView.as_view(dict(get='retrieve')), name="articles_articles"),
    url(r'^articles/article/images$', ArticleImagesView.as_view(dict(post='create')), name="articles_article_images"),
    url(r'^articles/article/(?P<alias>.*)$', ArticleView.as_view(dict(get='retrieve')), name="articles_article"),
)