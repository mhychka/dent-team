from django.db import models
from authentication.models import AuthUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class ContactMessage(models.Model):
    STATUS_PENDING = 'pending'
    STATUS_HANDLED = 'handled'
    STATUS_HANDLING = 'handling'
    STATUS_ERROR = 'error'

    name = models.CharField(max_length=255)
    subject = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    status = models.CharField(max_length=10, default=STATUS_PENDING, null=False)
    message = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    handling_attempt = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return self.email

    def data(self, lang=None):
        return {
            'id': self.pk,
            'name': self.name,
            'subject': self.name,
            'email': self.email,
            'message': self.message,
            'created': str(self.created) if self.created else None
        }

    def get_data(self, fields='all', lang=None):
        data = self.data(lang)
        allfields = fields == 'all'

        return data

    class Meta:
        verbose_name = _('Contact Message')
        verbose_name_plural = _('Contact Messages')