# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import sorl.thumbnail.fields
import authentication.utils


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthUser',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status', default=False)),
                ('image', sorl.thumbnail.fields.ImageField(max_length=255, null=True, upload_to=authentication.utils.generate_userimage_file_path)),
                ('username', models.CharField(max_length=255, unique=True)),
                ('email', models.EmailField(max_length=255, db_index=True, unique=True)),
                ('first_name', models.CharField(max_length=200, null=True, blank=True)),
                ('last_name', models.CharField(max_length=200, null=True, blank=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('date_joined', models.DateTimeField(auto_now_add=True)),
                ('phone', models.CharField(max_length=200, null=True, blank=True)),
                ('chat_id', models.CharField(max_length=255, null=True)),
                ('guid', models.CharField(max_length=10, null=True, unique=True)),
                ('language', models.CharField(max_length=4, null=True)),
                ('groups', models.ManyToManyField(related_query_name='user', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups', blank=True, to='auth.Group', related_name='user_set')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', help_text='Specific permissions for this user.', verbose_name='user permissions', blank=True, to='auth.Permission', related_name='user_set')),
            ],
            options={
                'verbose_name_plural': 'Users',
                'swappable': 'AUTH_USER_MODEL',
                'abstract': False,
                'verbose_name': 'User',
            },
        ),
        migrations.CreateModel(
            name='AuthUserData',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('chat_new_direct_messages', models.IntegerField(default=0)),
                ('chat_new_channel_messages', models.IntegerField(default=0)),
                ('chat_new_channel_mentions', models.IntegerField(default=0)),
                ('user', models.ForeignKey(unique=True, related_name='userdata', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'AuthUserData',
                'abstract': False,
                'verbose_name': 'AuthUserData',
            },
        ),
    ]
