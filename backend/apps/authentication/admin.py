# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .forms import AuthUserForm
from .models import AuthUser


class AuthUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'username', 'email')}),)
    add_fieldsets = (
        (None, {'fields': ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')}),)

    form = AuthUserForm
    add_form = AuthUserForm
    list_display = ('first_name', 'username', 'email')
    list_filter = ('is_active',)
    search_fields = ('email', 'username',)
    ordering = ('username',)


admin.site.register(AuthUser, AuthUserAdmin)
