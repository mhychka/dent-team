# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm, SetPasswordForm
from django.utils.translation import ugettext_lazy as _
from captcha.fields import ReCaptchaField
from authentication.models import AuthUser
from django.core.exceptions import ObjectDoesNotExist


class AuthUserForm(forms.ModelForm):
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match.")
    }

    email = forms.EmailField(max_length=64, widget=forms.TextInput(
        attrs={'placeholder': 'Email', 'type': 'email', 'class': 'form-control',
               "data-error": "Email address is invalid", "":""}))
    username = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'placeholder': '@username', 'type': 'text', 'class': 'form-control', 'required':'required',
               'data-error':'Incorrect field value.', }))
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'First Name', 'type': 'text', 'class': 'form-control', 'required':'required',
               'data-error':'This field may not be blank.'}))
    last_name = forms.CharField(max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Last Name', 'type': 'text', 'class': 'form-control', 'required':'required',
               'data-error':'This field may not be blank.'}))

    class Meta:
        model = AuthUser
        fields = ('email', 'username', 'first_name', 'last_name', 'phone')

    def clean(self):
        cleaned_data = super().clean()
        #print(self.errors)
        return cleaned_data


class AuthUserLoginForm(AuthenticationForm):
    pass


class AuthUserPasswordResetForm(forms.Form):
    error_messages = {
        'invalid_email': _("Please enter a correct email. User does not exist."),
    }
    email = forms.EmailField(max_length=64)

    def __init__(self, request = None, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')

        try:
            self.user = AuthUser.objects.get(email=email)
            return dict(email=email)
        except ObjectDoesNotExist:
            raise forms.ValidationError(
                    self.error_messages['invalid_email'],
                    code='invalid_login',
                    params={'email': email},
                )

    def save(self):
        from authentication.tasks import AuthUserSendResetPasswordEmailTask

        task = AuthUserSendResetPasswordEmailTask()
        task.apply_async((self.cleaned_data['email'], {'host':self.request.get_host()}))


class AuthUserPasswordSetForm(SetPasswordForm):
    pass