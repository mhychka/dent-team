# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from sorl.thumbnail.fields import ImageField
from authentication.utils import generate_userimage_file_path
from sorl.thumbnail import get_thumbnail, delete as delete_thumbnail
from django.conf import settings
from utils.string import guid


class AuthUserManager(BaseUserManager):
    def get_user_by_token(self, token):
        if not token:
            return None
        try:
            user = self.get(auth_token=token)
        except ObjectDoesNotExist:
            user = None
        return user


class AbstractAuthUser(AbstractBaseUser, PermissionsMixin):
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'

    class Meta:
        abstract = True


class AuthUser(AbstractAuthUser):
    FILE_UPLOAD_MAX_SIZE = 10
    FILE_UPLOAD_ALLOWED_TYPES = ('.jpeg', '.jpg', '.png')

    IMAGE_THUMBNAILS = {'s': '128x128', 'x': '256x256'}
    IMAGE_MIME_TYPES = ['image/jpeg', 'image/png']

    image = ImageField(max_length=255, null=True, upload_to=generate_userimage_file_path)
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255, unique=True, db_index=True)
    first_name = models.CharField(max_length=200, blank=True, null=True)
    last_name = models.CharField(max_length=200, blank=True, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    phone = models.CharField(max_length=200, blank=True, null=True)
    chat_id = models.CharField(max_length=255, null=True)
    guid = models.CharField(max_length=10, unique=True, null=True)
    language = models.CharField(max_length=4, null=True)
    objects = AuthUserManager()
    _paymentplanlinks = None
    _company = None

    def get_profile(self):
        return None

    @property
    def base_dir_path(self):
        return self.date_joined.strftime("users/%Y/%m/%d/{id}/").format(id=self.id)

    @classmethod
    def get_guid(cls):
        while True:
            try:
                g = guid()
                cls.objects.get(guid=g)
            except ObjectDoesNotExist:
                return g

    @property
    def thumbnails(self):
        urls = dict()
        for thumb_name, thumb_size in self.IMAGE_THUMBNAILS.items():
            urls[thumb_name] = self.get_thumbnail(thumb_name)

        return urls

    def get_thumbnail(self, thumb_name, host=None, protocol='http'):
        thumb_size = self.IMAGE_THUMBNAILS[thumb_name]
        if self.image and self.image.url:
            t = get_thumbnail(self.image, thumb_size)
            url = t.url
            if host:
                url = "{protocol}://{host}{url}".format(url=url, host=host, protocol=protocol)
            return dict(url=url, path=t.storage.path(t.name), width=t.width, height=t.height) \
                if t.size \
                else dict(url='', path='', width=0, height=0)
        elif thumb_name == 's':
            path = settings.STATIC_ROOT+'/img/user128x128.png'
            url = settings.STATIC_URL+'img/user128x128.png'
            if host:
                url = "{protocol}://{host}{url}".format(url=url, host=host, protocol=protocol)
            return dict(url=url, path=path, width=128, height=128)
        elif thumb_name == 'x':
            path = settings.STATIC_ROOT+'/img/user256x256.png'
            url = settings.STATIC_URL+'img/user256x256.png'
            if host:
                url = "{protocol}://{host}{url}".format(url=url, host=host, protocol=protocol)
            return dict(url=url, path=path, width=256, height=256)
        return (None, None, None, None)

    @property
    def name(self):
        if self.first_name and self.first_name.strip():
            return self.first_name \
                if not self.last_name \
                else self.first_name + ' ' + self.last_name

        if self.username and self.username.strip():
            return self.username

        if self.email and self.email.strip():
            return self.email

        return ''

    def get_short_name(self):
        return self.username

    def check_company_role(self, role):
        from companies.models import CompanyUserLink
        return self.company and self.company.check_user_role(self.id, role)

    @property
    def project(self):
        from projects.models import Project
        from django.db.models import Q
        if not self.is_authenticated():
            return None

        ids = ProjectUserLink.objects.values('project_id').filter(user=self)
        projects = Project.objects.filter(Q(id__in=ids) | Q(owner=self)).order_by('-created')[:1]
        return projects[0] if list(projects) else None

    @property
    def company(self):
        if self._company == None:
            companylinks = self.companylinks.all()
            created = None
            link = None
            for l in companylinks:
                if not created:
                    created = l.created

                if created and created <= l.created:
                    link = l
                    created = l.created

            company = link.company if link else None
            if not company:
                from companies.services.company import CompanyService
                company = CompanyService.create_company(self)

            self._company = company

        return self._company

    def in_group(self, name):
        names = [g.name for g in self.groups.all()]
        return name in names

    def notification_ids(self):
        from push_notifications.models import APNSDevice, GCMDevice
        ids = []
        for device in GCMDevice.objects.filter(user=self):
            ids.append(device.registration_id)
        return ids

    @property
    def data(self):
        return {
            'id': self.pk,
            'first_name': self.first_name,
            'username': self.username,
            'name': self.name,
            'email': self.email,
            'phone': self.phone,
            'date_joined': self.date_joined.strftime("%Y-%m-%d")
        }

    def get_userdata(self):
        if list(self.userdata.all()):
            return self.userdata.all()[0]

        return AuthUserData.objects.create(user=self)

    def get_data(self, fields=None):
        data = self.data

        if not fields:
            return data

        if 'groups' in fields:
            data['groups'] = [g.name for g in self.groups.all()]

        if 'thumbs' in fields:
            data['thumbs'] = self.thumbnails

        if 'company' in fields:
            data['company'] = self.company.get_data(['user'], self.id) if self.company else None

        if 'paymentplanlink' in fields:
            link = self.get_paymentplanlink()
            data['paymentplanlink'] = self.get_paymentplanlink().get_data(['plan']) if link else None

        return data

    def get_paymentplanlinks(self):
        if self._paymentplanlinks == None:
            company = self.company
            self._paymentplanlinks = list(self.paymentplanlinks.select_related('plan').prefetch_related('plan__quotalinks').filter(company=company, disabled__isnull=True))
        return self._paymentplanlinks

    def get_paymentplanlink(self):
        links = self.get_paymentplanlinks()
        if links:
            return links[0]

        from payments.services.company import PaymentCompanyService

        company = self.company
        plan = company.get_active_plan()

        if plan:
            service = PaymentCompanyService(company, self)
            link = service.activate_plan_for_user(plan, self, 'autoactivation')
            return link

        return None

    class Meta(AbstractAuthUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class AuthUserData(models.Model):
    user = models.ForeignKey('AuthUser', related_name='userdata', unique=True)
    chat_new_direct_messages = models.IntegerField(default=0)
    chat_new_channel_messages = models.IntegerField(default=0)
    chat_new_channel_mentions = models.IntegerField(default=0)

    @property
    def data(self):
        return {
            'id': self.pk,
            'user_id': self.user_id,
            'chat_new_direct_messages': self.chat_new_direct_messages,
            'chat_new_channel_messages': self.chat_new_channel_messages,
        }

    def get_data(self, fields=None):
        data = self.data
        return data

    class Meta(AbstractAuthUser.Meta):
        verbose_name = 'AuthUserData'
        verbose_name_plural = 'AuthUserData'