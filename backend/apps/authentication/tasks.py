from celery import Task
from utils import email as mail
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from authentication.models import AuthUser

# class AuthUserSendResetPasswordEmailTask(Task):
#     queue = 'default'
#     routing_key = 'default'
#
#     def run(self, email, params):
#         user = AuthUser.objects.get(email=email)
#         params.update({
#             'email': email,
#             'user': user,
#             'uidb64': urlsafe_base64_encode(force_bytes(user.pk)),
#             'token': default_token_generator.make_token(user),
#         })
#         email_params = {
#             'subject': 'COBASE: Reset account`s password',
#             'recipient_list': [email],
#             'template': 'authentication/email/password_reset.html',
#             'params':params
#         }
#
#         mail.send_html(**email_params)
