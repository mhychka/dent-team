from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core import exceptions
from django.contrib.auth.views import redirect_to_login

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class SuperUserRequiredMixin(object):
    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class GuestRequiredMixin(object):
    @method_decorator(user_passes_test(lambda u: not u.is_authenticated(), login_url="/"))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class OwnerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if obj.owner != self.request.user:
            raise exceptions.PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class UserGroupPManagerMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = self.request.user
        if not user.is_authenticated():
                return redirect_to_login(request.get_full_path())

        if not user.in_group('pmanager'):
            raise exceptions.PermissionDenied

        return super().dispatch(request, *args, **kwargs)