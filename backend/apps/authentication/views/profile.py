# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from authentication.views.mixins import LoginRequiredMixin
from django.conf import settings
from django.http import HttpResponseRedirect


class AuthUserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'authentication/profile/profile.html'

    def get_context_data(self, **kwargs):
        from companies.models import CompanyService

        data = super().get_context_data(**kwargs)
        data['user'] = self.request.user
        data['services'] = CompanyService.objects.all()
        return data


class AuthUserProfileCreateView(TemplateView):
    template_name='authentication/profile/registration.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['type'] = self.request.GET.get('type', None)
        data['email'] = self.request.GET.get('email', None)
        return data

    def dispatch(self, request, *args, **kwargs):
        redirect_url = request.GET.get('next', None)
        if not redirect_url:
            redirect_url= '/'
        if request.user.is_authenticated():
            return HttpResponseRedirect(redirect_url)

        return super().dispatch(request, *args, **kwargs)