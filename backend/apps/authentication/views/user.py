# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from authentication.views.mixins import *
from authentication.forms import *
from django.http import HttpResponseRedirect
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.conf import settings


class ResetUserPasswordView(TemplateView):
    template_name = 'authentication/user/password_reset_form.html'

    def get(self, request, *args, **kwargs):
        form = AuthUserPasswordResetForm()
        context = {'form': form}
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = AuthUserPasswordResetForm(request, request.POST)
        if form.is_valid():
            form.save()
            self.template_name = 'authentication/user/password_reset_done.html'
        context = {'form':form}
        return self.render_to_response(context)


class ResetUserPasswordConfirmView(TemplateView):
    template_name = 'authentication/user/password_reset_confirm.html'

    def get(self, request, *args, **kwargs):
        uidb64 = kwargs['uidb64']
        token = kwargs['token']

        assert uidb64 is not None and token is not None

        try:
            uid = urlsafe_base64_decode(uidb64)
            users = AuthUser.objects.filter(pk=uid)[:1]
            user = users[0] if list(users) else None
        except (TypeError, ValueError, OverflowError, AuthUser.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if request.method == 'POST':
                form = AuthUserPasswordSetForm(user, request.POST)
                if form.is_valid():
                    form.save()
                    return HttpResponseRedirect(reverse('authentication:password_reset_complete'))
            else:
                form = AuthUserPasswordSetForm(user)

            validlink = True
            title = _('Enter new password')
        else:
            validlink = False
            form = None
            title = _('Password reset unsuccessful')

        context = {'form': form, 'title': title, 'validlink': validlink}
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)


class ResetUserPasswordDoneView(TemplateView):
    template_name = 'authentication/user/password_reset_complete.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return self.render_to_response(context)