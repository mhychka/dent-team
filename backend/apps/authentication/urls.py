from django.conf.urls import patterns, url
from authentication.views.user import AuthUserLoginForm, ResetUserPasswordView, ResetUserPasswordDoneView, ResetUserPasswordConfirmView
from authentication.views.profile import AuthUserProfileCreateView, AuthUserProfileView

urlpatterns = patterns('',
    url('^login/$', 'django.contrib.auth.views.login',
        {'template_name': 'authentication/user/login.html', 'authentication_form': AuthUserLoginForm},
        name='login'),
    url('^login$', 'django.contrib.auth.views.login',
        {'template_name': 'authentication/user/login.html', 'authentication_form': AuthUserLoginForm},
        name='login'),
    url('^logout$', 'django.contrib.auth.views.logout', name='logout',
        kwargs={'template_name': 'authentication/user/logout.html', 'next_page':'/user/login'}),
    url('^register', AuthUserProfileCreateView.as_view(), name='register'),
    url('^password_change$', 'django.contrib.auth.views.password_change', name='password_change'),
    url('^password_change/done$', 'django.contrib.auth.views.password_change_done', name='password_change_done'),
    url('^password_reset$', ResetUserPasswordView.as_view(), name='password_reset'),
    url('^password_reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})$',
        ResetUserPasswordConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/done$', ResetUserPasswordDoneView.as_view(), name='password_reset_complete'),
    url('^password_reset/done$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    url('^(?P<username>[a-zA-Z0-9_.-]+)$', AuthUserProfileView.as_view(), name='profile')
)