from django.apps import AppConfig

class AuthUserConfig(AppConfig):

    name = 'authentication'
    verbose_name = "Auth Users"