from django.core.exceptions import ObjectDoesNotExist
from authentication.models import AuthUser
from django.db.models import Q
from django.utils import timezone
import datetime
import time


class AuthUserListService:

    def __init__(self, user):
        self.user = user

    def company_invite_users(self, params, query):
        field = params.pop('field', None)
        limit = 8
        users = []

        from companies.models import CompanyUserLink, CompanyUserInvite
        if field == 'email':
            _users = AuthUser.objects.\
                exclude(pk=self.user.id).\
                filter(Q(email__icontains=query))[:50]
        else:
            _users = AuthUser.objects.\
                exclude(pk=self.user.id).\
                filter(Q(first_name__icontains=query) | Q(username__icontains=query))[:50]

        links = CompanyUserLink.objects.select_related('user').prefetch_related('company__userlinks').\
            filter(user__in=[u.id for u in _users]).\
            filter(status=CompanyUserLink.STATUS_ACTIVE)

        users_dict = {u.email:u for u in _users}
        for l in links:
            if l.role == CompanyUserLink.ROLE_WORKER or len(l.company.get_userlinks(excluded_users=[l.user.id])) > 0:
                if l.user.email in users_dict:
                    del users_dict[l.user.email]

        invites = CompanyUserInvite.objects.filter(email__in=[user_email for user_email, user in users_dict.items()], accepted__isnull=True)
        for i in invites:
            if i.email in users_dict:
                    del users_dict[i.email]

        l = 0
        for user_email, user in users_dict.items():
            users.append(user)
            if l >= limit:
                break
            l+=1

        return users

    def project_manager_users(self, params, query):
        from companies.models import CompanyUserLink, CompanyUserInvite

        limit = 8
        company = self.user.company
        users = []

        links = CompanyUserLink.objects.select_related('user').\
            filter(Q(user__first_name__icontains=query) | Q(user__username__icontains=query) | Q(user__email__icontains=query)).\
            filter(company=company)[:limit]

        for l in links:
            users.append(l.user)

        return users

    def project_access_users(self, params, query):
        from companies.models import CompanyUserLink, CompanyUserInvite
        from projects.services.projectlist import ProjectsListService
        from projects.models import ProjectUserAccess

        company = self.user.company
        project = ProjectsListService.get_project(params['project'])
        limit = 8
        users = []

        workers = CompanyUserLink.objects.values('user_id').filter(role=CompanyUserLink.ROLE_WORKER)
        company_users = CompanyUserLink.objects.values('user_id').filter(company_id=company.id)
        project_users = [l.user.id for l in project.get_userlinks()]
        access_users = [a.user_id for a in ProjectUserAccess.objects.filter(project=project) if a.user_id]

        links = CompanyUserLink.objects.\
            select_related('user').\
            filter(Q(user__first_name__icontains=query) | Q(user__username__icontains=query) | Q(user__email__icontains=query)).\
            exclude(user_id__in=workers).\
            exclude(user_id__in=project_users).\
            exclude(user_id__in=company_users).\
            exclude(user_id__in=access_users)[:limit]

        for l in links:
            if l.user not in users:
                users.append(l.user)

        return users

    def project_company_users(self, params, query):
        from projects.models import ProjectUserLink
        from projects.services.projectlist import ProjectsListService

        limit = 8
        project_id = params.pop('project', None)
        project = ProjectsListService.get_project(project_id)
        company = self.user.company
        userlinks = project.get_userlinks(company_id=company.id)
        userids = [l.user_id for l in userlinks]
        data = []
        if project_id:
            links = ProjectUserLink.objects.select_related('user').\
                filter(Q(user__first_name__icontains=query) | Q(user__username__icontains=query) | Q(user__email__icontains=query)).\
                filter(user_id__in=userids).\
                filter(project_id=project_id,)[:limit]

            for l in links:
                data.append(l.user)

        return data

    def project_all_users(self, params, query):
        from projects.models import ProjectUserLink

        limit = 8
        project_id = params.pop('project', None)
        data = []
        if project_id:
            links = ProjectUserLink.objects.select_related('user').\
                filter(Q(user__first_name__icontains=query) | Q(user__username__icontains=query) | Q(user__email__icontains=query)).\
                filter(project_id=project_id,)[:limit]

            for l in links:
                data.append(l.user)

        return data

    def company_members(self, params, query):
        from companies.models import CompanyUserLink, CompanyUserInvite
        company = self.user.company
        limit = 8
        data = {}

        company_links = CompanyUserLink.objects.select_related('user').\
            filter(company_id=company.id, status=CompanyUserLink.STATUS_ACTIVE).\
            filter(Q(user__first_name__icontains=query) | Q(user__username__icontains=query) | Q(user__email__icontains=query))[:limit]

        company_invites = CompanyUserInvite.objects.\
            filter(company_id=company.id).\
            filter(Q(name__icontains=query) | Q(email__icontains=query))[:limit]

        l = 1
        for link in company_links:
            if l <= limit:
                data[link.user.email] = {'name': link.user.name, 'email': link.user.email}
                l += 1
            else:
                break

        if l <= limit:
            for invite in company_invites:
                if l <= limit and invite.email not in data:
                    data[invite.email] = {'name': invite.name, 'email': invite.email}
                    l += 1
                else:
                    break

        return [user_data for user_email, user_data in data.items()]

    def get_data(self, params):
        rtype = params.pop('type', None)
        query = params.pop('query', None)

        if rtype == 'cui':
            return self.company_invite_users(params, query)

        elif rtype == 'pman':
            return self.project_manager_users(params, query)

        elif rtype == 'cmem':
            return self.company_members(params, query)

        elif rtype == 'pacc':
            return self.project_access_users(params, query)

        elif rtype == 'pu':
            return self.project_company_users(params, query)

        elif rtype == 'apu':
            return self.project_all_users(params, query)

        return []


class AuthUserDataService:
    def __init__(self, user):
        self.user = user

    def get_data(self, fields='all', params=None):

        user = self.user
        data = {}
        allfields = fields == 'all'

        if (allfields or 'projectstasks' in fields) and self.user.is_authenticated():
            from projects.services.projectuser import ProjectUserDataService, ProjectsListService

            projects_list_service = ProjectsListService(self.user)
            projects, next_page = projects_list_service.get_data({'page_size': 10000000000})
            project_ids = [project.id for project in projects]

            project_user_data_service = ProjectUserDataService(user=self.user)
            projects_user_data = project_user_data_service.get_data_by_ids(project_ids, ['tasks'])
            data['projectstasks'] = projects_user_data['tasks']

        if (allfields or 'projecttasks_new_count' in fields) and self.user.is_authenticated():
            if 'projectstasks' in data:
                data['projecttasks_new_count'] = len(data['projectstasks']['all']['total']['recent'])

        if (allfields or 'chat_new_messages_count' in fields) and self.user.is_authenticated():
                userdata = user.get_userdata()
                data['chat'] = {
                    'new_direct_messages_count': userdata.chat_new_direct_messages,
                    'new_channel_messages_count': userdata.chat_new_channel_messages,
                    'new_channel_mentions_count': userdata.chat_new_channel_mentions,
                }

        if (allfields or 'projectuserlogs_pending' in fields) and self.user.is_authenticated() and self.user.company:
            from projects.services.projectlog import ProjectUserLogService
            service = ProjectUserLogService(user.id)
            tzinfo = user.company.tzinfo
            now = datetime.datetime.now(timezone.utc)
            now_date = now.astimezone(tzinfo).date()
            now_logs = service.get_date_logs(now_date, tzinfo)

            pending_logs = []
            for l in now_logs:
                if (l.get_start_geostatus() == 'pending' or l.get_end_geostatus() == 'pending') and l.start_time:
                    seconds = (now - l.start_time).total_seconds()
                    if seconds <= 240:
                        pending_logs.append(l)


            data['projectuserlogs'] = {'pending': [l.get_data('geostatus') for l in pending_logs]}

        return data


class AuthUserOnlineStatusService:
    USER_ONLINE_MINUTES = 5
    SUBTYPES = ['app', 'chat']

    def __init__(self, user, online_type):
        from utils.redis import get_redis_client
        self.user = user
        self.online_type = online_type
        self.redis_client = get_redis_client()

    # def mark_online_interval(self, online_type='app', timeout=USER_ONLINE_MINUTES):
    #     from utils.datetime import set_interval
    #     return set_interval(self.mark_online(online_type), timeout*60)

    def mark_online(self):
        now = int(time.time())
        expires = now + (self.USER_ONLINE_MINUTES * 60) + 10
        users_online_name = 'authentication:user_online_' + self.online_type + '/%d' % (now // 60)
        user_activity_name = 'authentication:user_online_activity_' + self.online_type + '/%s' % self.user.id

        p = self.redis_client.pipeline()
        p.sadd(users_online_name, self.user.id)
        p.set(user_activity_name, now)
        p.expireat(users_online_name, expires)
        p.expireat(user_activity_name, expires)
        p.execute()

    def mark_offline(self):
        current = int(time.time()) // 60
        minutes = iter(range(self.USER_ONLINE_MINUTES))
        names = [('authentication:user_online_' + self.online_type + '/%d' % (current - min)) for min in minutes]
        for name in names:
            self.redis_client.delete(name)

    def get_user_last_activity(self):
        name = 'authentication:use_online_activity_' + self.online_type + '/%s' + self.user.id
        last_active = self.redis_client.get(name)
        return datetime.datetime.utcfromtimestamp(int(last_active)) if last_active else None

    def get_online_users(self):
        current = int(time.time()) // 60
        minutes = iter(range(self.USER_ONLINE_MINUTES))
        names = [('authentication:user_online_' + self.online_type + '/%d' % (current - min)) for min in minutes]
        return [int(id) for id in self.redis_client.sunion(names)]

    def is_user_online(self):
        return self.user.id in self.get_online_users()