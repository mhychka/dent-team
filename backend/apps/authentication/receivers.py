from django.db.models.signals import post_save, post_delete
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver
from authentication.models import AuthUser
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import Group
import datetime
from django.utils import timezone
from django.conf import settings