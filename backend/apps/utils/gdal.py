from PIL import Image
import math
import os


def make_tiles(img_path, tile_path, tile_size = 256):
    def adjustBounds(src_img):
        src_width, src_height = src_img.size
        target_width = src_width + (tile_size - src_width % tile_size)
        target_height = src_height + (tile_size - src_height % tile_size)
        target_img = Image.new('RGBA', (target_width, target_height))
        target_img.paste(src_img, (0, 0))
        return target_img

    img = Image.open(img_path)
    w, h = img.size

    mzoom = max_zoom(width=w, height=h, tile_size=tile_size)

    for z in range(mzoom, -1, -1):
        adjusted_image = adjustBounds(img)
        numcolumns = adjusted_image.size[0] / tile_size
        numrows = adjusted_image.size[1] / tile_size

        for x in range(math.ceil(numcolumns)):
            path = os.path.join(tile_path, str(z), str(x))
            if not os.path.isdir(path):
                os.makedirs(path)

            for y in range(math.ceil(numrows)):
                bounds = (x * tile_size, y * tile_size, (x + 1) * tile_size,
                          (y + 1) * tile_size)
                tile = adjusted_image.crop(bounds)
                tile.save('%s/%s.png' % (path, y))

        w, h = img.size
        img = img.resize((math.ceil(w / 2), math.ceil(h / 2)), Image.ANTIALIAS)

def max_zoom(width=None, height=None, tile_size=256):
    return int(math.ceil(math.log((max(width, height) / tile_size), 2)))

# def make_tiles(img_path, tile_path):
#     cmd = 'gdal2tiles.py -p raster -z 0-5 -w none -n {img_path} {tile_path}'.format(
#         img_path=img_path, tile_path=tile_path)
#
#     os.system(cmd)
