from django.conf import settings
import os
from utils.filesystem import makedir
from django.conf import settings
from pipeline.compilers import CompilerBase
from pipeline.exceptions import CompilerError
from pipeline.compilers.es6 import ES6Compiler
from utils.string import guid


def compile_es6(source_url, extension='es6'):
    extension = '.'+extension
    path = source_url.replace('/static/', '')
    source_path = settings.STATIC_ROOT + '/' + path
    out_path = settings.STATIC_ROOT + '/es5/' + path
    out_path = out_path.replace(extension, '.js')
    out_dir = os.path.dirname(out_path)
    out_url = settings.STATIC_URL + 'es5/' + path
    out_url = out_url.replace(extension, '.js')
    compile = True
    source_modified = os.path.getmtime(source_path)
    if os.path.exists(out_path):
        out_modified = os.path.getmtime(out_path)
        compile = out_modified != source_modified
    if compile:
        makedir(out_dir)
        cmd = 'babel {source_path} --out-file {out_path}'.format(source_path=source_path, out_path=out_path)
        os.system(cmd)
        if os.path.exists(out_path):
            os.utime(out_path, (source_modified, source_modified))

    return out_url, compile

def pipeline_css(package_name, request, cache=True):
    from pipeline.templatetags.pipeline import  StylesheetNode
    from main.settings.css import packages_css

    compressed = not settings.DEBUG

    user = request.user
    email = request.user.email if user.is_authenticated() else None
    if email:
        for dev_name, dev_email in settings.DEVELOPERS:
            if email == dev_email:
                compressed = False

    if compressed:
        node = StylesheetNode(package_name)
        return node.render({package_name:package_name})
    else:
        package = packages_css[package_name]
        html = ''
        for path in package['source_filenames']:
            src = '/static/'+ path

            if not cache:
                src += '?g='+guid()
            html += '<link rel="stylesheet" type="text/css" href="{href}" />'.format(href=src) + "\n"
        return html

def webpack_js(bundle_name, request, cache=True):
    compressed = not settings.DEBUG

    user = request.user
    email = request.user.email if user.is_authenticated() else None
    if email:
        for dev_name, dev_email in settings.DEVELOPERS:
            if email == dev_email:
                compressed = False

    if compressed:
        src = '/static/js/compressed/{bundle_name}.compressed.{build_version}.bundle.js'.format(bundle_name=bundle_name, build_version=settings.BUILD_VERSION)
    else:
        src = '/static/js/bundles/{bundle_name}.webpack.bundle.js'.format(bundle_name=bundle_name)
        if not cache:
            src += '?g='+guid()

    return '<script src={src}></script>'.format(src=src)

def webpack_js_i18n(request, cache=True):
    langs = settings.LANGUAGES
    scripts = ""
    for lang in langs:
        lang_code = lang[0]
        scripts += webpack_js('i18n-'+lang_code, request, cache)

    return scripts