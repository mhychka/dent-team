import os


def makedir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def delfile(path):
    if os.path.exists(path):
        os.remove(path)

def deldir(path):
    import shutil
    return shutil.rmtree(path)

def copy(src, dst):
    import shutil, errno

    try:
        shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise