
def format_seconds(seconds):
    if seconds > 0:
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        return "%d:%02d" % (h, m)

    return "00:00"

def set_interval(func, sec):
    import threading

    def func_wrapper():
        set_interval(func, sec)
        func()

    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t
