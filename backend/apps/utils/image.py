import os, sys
from PIL import Image
from utils.filesystem import makedir
import ntpath
from django.conf import settings

def make_thumbnails(file, thumbs):
    result = []

    dir, filename = os.path.split(file.path)
    thumb_dir = dir + '/thumbnails'
    makedir(thumb_dir)
    width = None
    height = None

    for thumb_name, thumb_size in thumbs.items():
        image = Image.open(file.path)
        width, height = image.size
        x, y = [int(x) for x in thumb_size.split('x')]
        thumb_filename = thumb_name.lower() + '_' + filename
        thumb_path = os.path.join(thumb_dir, thumb_filename)
        if not os.path.exists(thumb_path):
            image.thumbnail([x, y], Image.ANTIALIAS)
            try:
                image.save(thumb_path, image.format, quality=100, optimize=1)
            except:
                image.save(thumb_path, image.format, quality=100)

        result.append(thumb_path)

    return (width, height, result)

def make_square_thumbnail(size, source_path, target_path):
    size = [size, size]
    image = Image.open(source_path)
    image.thumbnail(size, Image.ANTIALIAS)
    background = Image.new('RGBA', size, (255, 255, 255, 0))
    background.paste(image, ((size[0] - image.size[0]) // 2, (size[1] - image.size[1]) // 2))
    background.save(target_path, 'png', quality=99)

def sample_image(percentage, source_path, target_path):
    cmd = 'convert -sample {percentage}% {source_path} {target_path}'.format(
        source_path=source_path,target_path=target_path, percentage=percentage)
    os.system(cmd)

def get_image_size(path):
    image = Image.open(path)
    return image.size

def pdf2jpg(source_path, target_path, page, size):
    target_dir, target_filename = ntpath.split(target_path)
    target_basename, target_ext = os.path.splitext(target_filename)
    target_temp_path = '{dir}/{bname}_temp{ext}'.format(
        dir=target_dir, bname=target_basename, ext=target_ext)
    cmd = 'pdftoppm -r 300 -f {page} {source_path} > {target_path}'.format(source_path=source_path,target_path=target_temp_path, page=page) \
        if settings.SERVER_PLATFORM == 'linux'\
        else 'sips -s format png {source_path} --out {target_path}'.format(source_path=source_path,target_path=target_temp_path, page=page)

    os.system(cmd)

    if not os.path.exists(target_temp_path):
        return False

    width, height = get_image_size(target_temp_path)
    if width > size:
        im = Image.open(target_temp_path)
        im.thumbnail((size,size), Image.ANTIALIAS)
        im.save(target_path, "PNG")
        os.remove(target_temp_path)
    else:
        os.rename(target_temp_path, target_path)

    return True