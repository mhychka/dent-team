import os
import random
import json

def strtodict(str):
    return dict(x.split(":") for x in str.split(','))

def strval(value):
    return "" if value is None else str(value)

def guid(length=10):
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    guid = ""

    for i in range(length):
        next_index = random.randrange(len(alphabet))
        guid += alphabet[next_index]

    return guid

def jsonstr2data(value):

    if type(value) is dict:
        return value
    elif type(value) is str:
        try:
            value = value.replace("'", '"')
            return json.loads(value)
        except ValueError:
            pass

    return dict()

def truncate_string(string, maxlen=50):
    string = str(string)
    if len(string) > maxlen:
        return string[0:50]+'...'

    return string

def extract_email(string):
    import re

    match = re.search(r'[\w\.-]+@[\w\.-]+', string)
    return match.group(0)

def replace_images(images, string, lang):
    i = 0
    if not string:
        return string

    for img in images:
        thumbs = img.thumbnails
        for thumb_name, thumb_data in thumbs.items():
            var = '{{img[{i}][{thumb_name}]}}'.format(i=i, thumb_name=thumb_name)
            string = string.replace(var, thumb_data['url'])
        var = '{{image-{i}}}'.format(i=i)
        string = string.replace(var, thumbs['m2']['url'])
        var = '{{img[{i}][title]}}'.format(i=i)
        title = img.title(lang)
        string = string.replace(var, title if title else '')
        var = '{{img[{i}][descr]}}'.format(i=i)
        description = img.description(lang)
        string = string.replace(var, description if description else '')
        i += 1

    return string

