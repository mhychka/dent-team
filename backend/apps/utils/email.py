from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.core.mail.message import EmailMultiAlternatives, EmailMessage
from django.core.mail import get_connection


def send_html(subject, recipient_list, params, template, from_email=None, cc_email=None, reply_to=None):
    if not from_email:
        from_email = settings.DEFAULT_FROM_EMAIL

    html = render_to_string(template, params)
    connection = get_connection(username=None, password=None, fail_silently=False)
    mail = EmailMultiAlternatives(subject, '', from_email, recipient_list, connection=connection, cc=cc_email, reply_to=reply_to)
    mail.attach_alternative(html, 'text/html')
    return mail.send()