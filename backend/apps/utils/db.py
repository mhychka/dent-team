import math


class SimplePaginatior:

    def __init__(self, page, page_size, count):
        self.count = int(count)
        self.page_size = int(page_size)
        self.page = int(page)

    @property
    def offset(self):
        return (self.page * self.page_size) - self.page_size

    @property
    def pages(self):
        return math.ceil(self.count/self.page_size)

    @property
    def next_page_number(self):
        if self.pages > 0 and self.page < self.pages:
            return (self.page + 1)

        return None

def list_to_string(list):
    str = ""
    for s in list:
        str += "'{s}',".format(s=s)
    return str[:-1]