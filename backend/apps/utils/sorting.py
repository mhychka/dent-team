
def attr_str_sort(attr):
    def kicker(obj):
        return getattr(obj, attr).lower()
    return kicker