from articles.models import Article, ArticleCategory
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


class BlogPostListService(object):

    def get_data(self, params):
        last_item_id = params.get('last_item_id', None)
        category = params.get('category', None)
        filter = {}

        if (last_item_id):
            last_item = BlogPost.objects.get(pk=last_item_id)
            filter['created__lt'] = last_item.created

        if (category):
            filter['category__alias'] = category
        queryset = BlogPost.objects.filter(**filter)
        page_size = params.get('page_size', 10)
        paginator = Paginator(queryset, page_size)
        page = params.get('page', 1)

        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        return [data, paginator]