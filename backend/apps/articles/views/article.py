# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.conf import settings
import json


class ArticleListView(TemplateView):
    template_name = 'articles/article/list.html'
    category = None

    def get_context_data(self, **kwargs):
        pass
        # kwargs['category'] = kwargs.get('category', self.category)
        # service = BlogPostListService()
        # blogposts, blogpost_paginator = service.get_data(kwargs)
        # categories = BlogCategory.objects.all()
        # text = Text.objects.get(alias='blog')
        # return {
        #     'params': json.dumps(kwargs),
        #     'categories': categories,
        #     'blogposts': blogposts,
        #     'blogpost_paginator': blogpost_paginator,
        #     'text': text
        # }


class ArticleView( TemplateView):
    template_name = 'articles/article/view.html'

    def get_context_data(self, **kwargs):
        pass
        # blogpost = self.get_object()
        # categories = BlogCategory.objects.all()
        #
        # content_type = ContentType.objects.get_for_model(blogpost)
        # comment_form = CommentForm(self.request, initial={'object_id': blogpost.id, 'content_type': content_type.id}, prefix='comment')
        # service = CommentListService()
        # comments, comment_paginator = service.get_data({'content_object': blogpost})
        #
        # return{
        #     'categories': categories,
        #     'blogpost': blogpost,
        #     'model': blogpost,
        #     'comments': comments,
        #     'comment_form': comment_form,
        #     'comment_paginator': comment_paginator
        # }