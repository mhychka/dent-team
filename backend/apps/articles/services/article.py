from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from articles.models import Article, ArticleCategory
from django.db.models import Q


class ArticlesListService:
    def __init__(self, user):
        self.user = user

    def get_data(self, params):
        filter = {}

        page = params.pop('page', 1)
        page_size = params.pop('page_size', 5)
        category_alias = params.pop('category', None)
        aliases = params.pop('aliases', None)
        order_by = params.pop('order', '-created').split(',')

        tag_codename = params.pop('tag', None)

        if category_alias:
            filter['category__alias'] = category_alias

        if aliases:
            filter['alias__in'] = aliases.split(',')

        if tag_codename:
            filter['tags__codename'] = tag_codename

        qs = Article.objects.\
            select_related('owner', 'category').\
            prefetch_related('images', 'tags').\
            filter(**filter).\
            order_by('-created')

        paginator = Paginator(qs, page_size)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)

        next_page_number = None
        if data.has_next():
            next_page_number = data.next_page_number()

        return [data, next_page_number]