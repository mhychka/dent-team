from django.db import models
from authentication.models import AuthUser
from django.utils.translation import ugettext_lazy as _
from tinymce import models as tinymce_models
from django.conf import settings
from sorl.thumbnail.fields import ImageField
from articles.utils import generate_article_image_path
from copy import copy
from sorl.thumbnail import get_thumbnail, delete as delete_thumbnail
from utils.string import guid, replace_images


class ArticleCategory(models.Model):
    title_uk = models.CharField(max_length=255)
    description_uk = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    alias = models.CharField(max_length=255, unique=True, blank=False, null=False)

    def title(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'title_'+lang
        return getattr(self, property)

    def description(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'description_'+lang
        return getattr(self, property)

    def __str__(self):
        return self.alias

    def data(self, lang=None):
        return {
            'id': self.pk,
            'title': self.title(lang),
            'alias': self.alias
        }

    class Meta:
        ordering = ['-created']
        verbose_name = _('Article Category')
        verbose_name_plural = _('Article Categories')


class Article(models.Model):
    title_uk = models.CharField(max_length=255, blank=True, null=True)
    ref_title_uk = models.CharField(max_length=255, blank=True, null=True)
    ref_preview_uk = models.CharField(max_length=255, blank=True, null=True)
    preview_uk = tinymce_models.HTMLField(blank=True, null=True)
    text_uk = tinymce_models.HTMLField(blank=True, null=True)
    alias = models.CharField(max_length=255, unique=True, blank=False, null=False, default='')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, null=True)
    category = models.ForeignKey(ArticleCategory, null=True)
    owner = models.ForeignKey(AuthUser, blank=True, null=True)
    meta_keywords_uk = models.TextField(blank=True, null=True)
    meta_description_uk = models.TextField(blank=True, null=True)
    tags = models.ManyToManyField('ArticleTag', through='ArticleTagLink')

    @property
    def base_dir_path(self):
        return self.created.strftime("articles/{alias}/").format(alias=self.alias)

    def title(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'title_'+lang
        return getattr(self, property)

    def ref_title(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'ref_title_'+lang
        return getattr(self, property)

    def ref_preview(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'ref_preview_'+lang
        return getattr(self, property)

    def preview(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'preview_'+lang
        return getattr(self, property)

    def preview_formatted(self, lang=None):
        return replace_images(self.get_images(), self.preview(lang), lang)

    def text(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'text_'+lang
        return getattr(self, property)

    def text_formatted(self, lang=None):
        return replace_images(self.get_images(), self.text(lang), lang)

    def meta_keywords(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'meta_keywords_'+lang
        return getattr(self, property)

    def meta_description(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'meta_description_'+lang
        return getattr(self, property)

    def get_images(self):
        images = self.images.all()
        return images

    def data(self, lang=None):
        return {
            'id': self.pk,
            'title': self.title(lang),
            'ref_title': self.ref_title(lang),
            'ref_preview': self.ref_preview(lang),
            'preview': self.preview(lang),
            'meta_keywords': self.meta_keywords(lang),
            'alias': self.alias,
            'created': str(self.created) if self.created else None
        }

    def get_data(self, fields='all', lang=None):
        data = self.data(lang)
        allfields = fields == 'all'

        if 'images' in fields or allfields:
            data['images'] = []
            images = self.get_images()
            for image in images:
                data['images'].append(image.get_data(['thumbs'], lang))

        if 'tags' in fields or allfields:
            data['tags'] = []
            tags = self.tags.all()
            for tag in tags:
                data['tags'].append(tag.data(lang))

        if 'preview_formatted' in fields or allfields:
            data['preview_formatted'] = self.preview_formatted(lang)

        if 'text_formatted' in fields or allfields:
            data['text_formatted'] = self.text_formatted(lang)

        if 'category' in fields or allfields:
            data['category'] = self.category.data(lang) if self.category else None

        return data

    def __unicode__(self):
        return self.alias

    def __str__(self):
        return self.alias

    class Meta:
        ordering = ['-created']
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')


class ArticleTag(models.Model):

    title_uk = models.CharField(max_length=255, blank=True, null=True)
    codename = models.CharField(max_length=255, unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def title(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'title_'+lang
        return getattr(self, property)

    def data(self, lang=None):
        return {
            'id': self.pk,
            'title': self.title(lang),
            'codename': self.codename,
        }

    def get_data(self, fields='all', lang=None):
        data = self.data(lang)
        return data

    def __unicode__(self):
        return self.codename

    def __str__(self):
        return self.codename

    class Meta:
        ordering = ['-created',]
        verbose_name = 'Article Tag'
        verbose_name_plural = 'Article Tags'


class ArticleTagLink(models.Model):

    article = models.ForeignKey(Article, related_name='taglinks')
    tag = models.ForeignKey(ArticleTag)

    def data(self, lang=None):
        return {
            'article_id': self.article_id,
            'tag_id': self.tag_id
        }

    def get_data(self, fields='all', lang=None):
        data = self.data(lang)
        return data

    class Meta:
        verbose_name = 'Article Tag Link'
        verbose_name_plural = 'Article Tag Links'


class ArticleImage(models.Model):
    SOURCE_LOCAL = 'local'
    FILE_UPLOAD_MAX_SIZE = 45
    FILE_UPLOAD_ALLOWED_TYPES = ('.jpeg', '.jpg', '.png', '.pdf')

    IMAGE_THUMBNAILS = {'s': '70x70', 'm': '1024x360', 'm2': '800x600'}
    IMAGE_MIME_TYPES = ['image/jpeg', 'image/png']

    STATUS_PENDING = 'pending'
    STATUS_HANDLING = 'handling'
    STATUS_READY = 'ready'
    STATUS_ERROR = 'error'

    source_messages = (
        (SOURCE_LOCAL, 'Local'),
    )

    status_messages = (
        (STATUS_PENDING, _('pending')),
        (STATUS_HANDLING, _('handling')),
        (STATUS_READY, _('ready')),
        (STATUS_ERROR, _('error')))

    file = ImageField(max_length=255, upload_to=generate_article_image_path)
    title_uk = models.CharField(max_length=255, blank=True, null=True)
    description_uk = models.CharField(max_length=255, blank=True, null=True)
    source_filename = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    filename = models.CharField(max_length=255, null=True)
    mime_type = models.CharField(max_length=30, null=True)
    guid = models.CharField(max_length=10, null=True, unique=True)
    source = models.CharField(max_length=20, null=True, default=SOURCE_LOCAL)
    size = models.PositiveIntegerField(null=True)
    width = models.PositiveIntegerField(null=True)
    height = models.PositiveIntegerField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    priority = models.SmallIntegerField(max_length=3, default=0)
    status = models.CharField(max_length=10, null=True, choices=status_messages)
    owner = models.ForeignKey(AuthUser, null=True)
    article = models.ForeignKey(Article, related_name='images')

    def title(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'title_'+lang
        return getattr(self, property)

    def description(self, lang=None):
        if not lang:
            lang = settings.LANGUAGE_CODE
        property = 'description_'+lang
        return getattr(self, property)

    @property
    def status_message(self):
        for id, message in self.status_messages:
            if id == self.status:
                return message
        return None

    @property
    def thumbnails(self):
        urls = dict()
        thumbs = copy(self.IMAGE_THUMBNAILS)
        for thumb_name, thumb_size in thumbs.items():
            urls[thumb_name] = self.get_thumbnail(thumb_name)

        return urls

    def get_thumbnail(self, thumb_name):
        thumb_size = self.IMAGE_THUMBNAILS[thumb_name]
        x, y = [int(x) for x in thumb_size.split('x')]

        if not self.width:
            self.width = 0
        if not self.height:
            self.height = 0

        if y > self.height:
            y = self.height
        if x > self.width:
            x = self.width

        size = 'x' + str(y)
        t = get_thumbnail(self.file, size)
        return dict(url=t.url, path=t.storage.path(t.name), width=t.width, height=t.height) \
            if t.size \
            else dict(url='', path='', width=0, height=0)

    @property
    def base_dir_path(self):
        return self.article.base_dir_path + 'images/'

    @classmethod
    def get_guid(cls):
        while True:
            try:
                g = guid()
                cls.objects.get(guid=g)
            except cls.DoesNotExist:
                return g

    def data(self, lang=None):
        return {
            'id': self.pk,
            'filename': self.filename,
            'title': self.title(lang),
            'description': self.description(lang),
            'name': self.name,
            'guid': self.guid,
            'source_filename': self.source_filename,
            'source': self.source,
            'created': str(self.created) if self.created else None ,
            'size': self.size,
            'width': self.width,
            'height': self.height,
            'owner_id': self.owner_id,
            'article_id': self.article_id,
            'type':'image',
            'status':self.status,
            'status_message':self.status_message,
            'mime_type': self.mime_type,
        }

    def get_data(self, fields='all', lang=None):
        data = self.data(lang)
        allfields = fields == 'all'

        if 'thumbs' in fields or allfields:
            data['thumbs'] = {n:{'src':v['url'], 'width': v['width'], 'height':v['height']} for (n,v) in self.thumbnails.items()}

        return data

    def __unicode__(self):
        return self.file

    class Meta:
        ordering = ['-created',]
        verbose_name = 'Article Image'
        verbose_name_plural = 'Article Images'