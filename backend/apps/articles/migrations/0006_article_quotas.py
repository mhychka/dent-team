# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0005_articletag_articletaglink'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='quotas',
            field=models.ManyToManyField(through='articles.ArticleTagLink', to='articles.ArticleTag'),
        ),
    ]
