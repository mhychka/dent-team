# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_articleimage_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleTag',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('title_uk', models.CharField(blank=True, max_length=255, null=True)),
                ('title_de', models.CharField(blank=True, max_length=255, null=True)),
                ('codename', models.CharField(unique=True, max_length=255)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'Article Tag',
                'ordering': ['-created'],
                'verbose_name_plural': 'Article Tags',
            },
        ),
        migrations.CreateModel(
            name='ArticleTagLink',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('article', models.ForeignKey(to='articles.Article', related_name='taglinks')),
                ('tag', models.ForeignKey(to='articles.ArticleTag')),
            ],
            options={
                'verbose_name': 'Article Tag Link',
                'verbose_name_plural': 'Article Tag Links',
            },
        ),
    ]
