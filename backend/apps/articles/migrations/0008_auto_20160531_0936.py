# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0007_auto_20160405_0730'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='meta_description_de',
        ),
        migrations.RemoveField(
            model_name='article',
            name='meta_keywords_de',
        ),
        migrations.RemoveField(
            model_name='article',
            name='preview_de',
        ),
        migrations.RemoveField(
            model_name='article',
            name='ref_preview_de',
        ),
        migrations.RemoveField(
            model_name='article',
            name='ref_title_de',
        ),
        migrations.RemoveField(
            model_name='article',
            name='text_de',
        ),
        migrations.RemoveField(
            model_name='article',
            name='title_de',
        ),
        migrations.RemoveField(
            model_name='articlecategory',
            name='description_de',
        ),
        migrations.RemoveField(
            model_name='articlecategory',
            name='title_de',
        ),
        migrations.RemoveField(
            model_name='articleimage',
            name='description_de',
        ),
        migrations.RemoveField(
            model_name='articleimage',
            name='title_de',
        ),
        migrations.RemoveField(
            model_name='articletag',
            name='title_de',
        ),
    ]
