# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title_uk', models.CharField(blank=True, max_length=255, null=True)),
                ('title_de', models.CharField(blank=True, max_length=255, null=True)),
                ('ref_title_uk', models.CharField(blank=True, max_length=255, null=True)),
                ('ref_title_de', models.CharField(blank=True, max_length=255, null=True)),
                ('preview_uk', tinymce.models.HTMLField(blank=True, null=True)),
                ('preview_de', tinymce.models.HTMLField(blank=True, null=True)),
                ('text_uk', tinymce.models.HTMLField(blank=True, null=True)),
                ('text_de', tinymce.models.HTMLField(blank=True, null=True)),
                ('alias', models.CharField(default='', max_length=255, unique=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(null=True, auto_now=True)),
                ('meta_keywords_uk', models.CharField(blank=True, max_length=255, null=True)),
                ('meta_keywords_de', models.CharField(blank=True, max_length=255, null=True)),
                ('meta_description_uk', models.TextField(blank=True, null=True)),
                ('meta_description_de', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Article',
                'verbose_name_plural': 'Articles',
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='ArticleCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('title_uk', models.CharField(max_length=255)),
                ('title_de', models.CharField(max_length=255)),
                ('description_uk', models.TextField(blank=True, null=True)),
                ('description_de', models.TextField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('alias', models.CharField(max_length=255, unique=True)),
            ],
            options={
                'verbose_name': 'Article Category',
                'verbose_name_plural': 'Article Categories',
                'ordering': ['-created'],
            },
        ),
        migrations.AddField(
            model_name='article',
            name='category',
            field=models.ForeignKey(null=True, to='articles.ArticleCategory'),
        ),
        migrations.AddField(
            model_name='article',
            name='owner',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
