# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0008_auto_20160531_0936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='meta_keywords_uk',
            field=models.TextField(blank=True, null=True),
        ),
    ]
