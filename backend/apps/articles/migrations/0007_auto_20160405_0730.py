# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0006_article_quotas'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='quotas',
        ),
        migrations.AddField(
            model_name='article',
            name='ref_preview_de',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='article',
            name='ref_preview_uk',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='article',
            name='tags',
            field=models.ManyToManyField(through='articles.ArticleTagLink', to='articles.ArticleTag'),
        ),
    ]
