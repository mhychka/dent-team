# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import articles.utils
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('articles', '0002_auto_20160320_2321'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', sorl.thumbnail.fields.ImageField(max_length=255, upload_to=articles.utils.generate_article_image_path)),
                ('title_uk', models.CharField(blank=True, max_length=255, null=True)),
                ('title_de', models.CharField(blank=True, max_length=255, null=True)),
                ('description_uk', models.CharField(blank=True, max_length=255, null=True)),
                ('description_de', models.CharField(blank=True, max_length=255, null=True)),
                ('source_filename', models.CharField(max_length=255, null=True)),
                ('filename', models.CharField(max_length=255, null=True)),
                ('mime_type', models.CharField(max_length=30, null=True)),
                ('guid', models.CharField(max_length=10, null=True, unique=True)),
                ('source', models.CharField(max_length=20, null=True, default='local')),
                ('size', models.PositiveIntegerField(null=True)),
                ('width', models.PositiveIntegerField(null=True)),
                ('height', models.PositiveIntegerField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('priority', models.SmallIntegerField(max_length=3, default=0)),
                ('status', models.CharField(max_length=10, null=True, choices=[('pending', 'pending'), ('handling', 'handling'), ('ready', 'ready'), ('error', 'error')])),
                ('article', models.ForeignKey(to='articles.Article', related_name='images')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-created'],
                'verbose_name': 'Article Image',
                'verbose_name_plural': 'Article Images',
            },
        ),
    ]
