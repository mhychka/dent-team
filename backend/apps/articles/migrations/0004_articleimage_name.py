# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_articleimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='articleimage',
            name='name',
            field=models.CharField(blank=True, null=True, max_length=255),
        ),
    ]
