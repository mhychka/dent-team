from django.contrib import admin
from articles.models import Article, ArticleCategory, ArticleImage, ArticleTag, ArticleTagLink
from mce_filebrowser.admin import MCEFilebrowserAdmin
from django.contrib.admin.options import InlineModelAdmin, flatten_fieldsets
from administration.widgets import AdminImageWidget
from django.db import models
from django.utils.safestring import mark_safe


class ArticleImageInline(InlineModelAdmin):
    template = 'administration/inline/images.html'
    model = ArticleImage
    extra = 0
    fields = ('file', 'title_uk', 'description_uk', 'priority')
    formfield_overrides = {models.ImageField: {'widget': AdminImageWidget}}
    upload_url = '/api/v1/articles/article/images'


class ArticleTagInline(admin.TabularInline):
    model = ArticleTagLink
    fields = ('tag',)


class ArticleTagAdmin(admin.ModelAdmin):
    fields = ('title_uk', 'codename',)
    list_display = ('title_uk', 'codename')


class ArticleAdmin(MCEFilebrowserAdmin):
    #inlines = [ArticleTagInline, ArticleImageInline]
    inlines = [ArticleImageInline,]
    list_display = ('title_uk', 'created')
    list_filter = ('created',)
    search_fields = ('title_uk',)
    ordering = ('-created',)


    def get_fields(self, request, obj=None):
        if(obj):
            return ('alias', 'reference', 'title_uk', 'meta_keywords_uk', 'text_uk', 'text_formatted_uk')
        return ('alias', 'title_uk', 'meta_keywords_uk', 'text_uk')

    def text_formatted_uk(self, obj):
        value = obj.text_formatted('uk')
        value = value.replace('\r\n', '')
        value = value.replace('\n', '')
        return mark_safe(value)

    def reference(self, obj):
        return '/app/articles/'+obj.alias

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ('reference', 'text_formatted_uk')
        return readonly_fields

class ArticleCategoryAdmin(MCEFilebrowserAdmin):
    list_display = ('title_uk', 'created')
    list_filter = ('created',)
    search_fields = ('title_uk', 'created')
    date_hierarchy = 'created'
    ordering = ('-created',)
    fields = ('title_uk', 'alias')


admin.site.register(Article, ArticleAdmin)
#admin.site.register(ArticleCategory, ArticleCategoryAdmin)
#admin.site.register(ArticleTag, ArticleTagAdmin)