from unidecode import unidecode
import re

def generate_text_image_path(instance, filename):
    filename = handle_filename(filename)
    return "{base_dir_path}{filename}".format(
        filename=filename, base_dir_path=instance.base_dir_path)

def handle_filename(filename):
    filename = unidecode(filename).lower()
    filename = re.sub("^\s+|\n|\r|\s+$", '', filename)
    filename = '_'.join(filename.split())
    filename = re.sub("[^a-z0-9_.-]", '', filename)
    return filename