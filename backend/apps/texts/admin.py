from django.contrib import admin
from texts.models import Text, TextImage
from mce_filebrowser.admin import MCEFilebrowserAdmin
from django.contrib.admin.options import InlineModelAdmin, flatten_fieldsets
from administration.widgets import AdminImageWidget
from django.db import models
from django.utils.safestring import mark_safe


class TextImageInline(InlineModelAdmin):
    template = 'administration/inline/images.html'
    model = TextImage
    extra = 0
    fields = ('file', 'title_uk', 'description_uk', 'link','priority')
    formfield_overrides = {models.ImageField: {'widget': AdminImageWidget}}
    upload_url = '/api/v1/texts/text/images'

class TextAdmin(MCEFilebrowserAdmin):
    inlines = [TextImageInline]
    list_display = ('title_uk', 'category', 'priority')
    list_filter = ('created',)
    search_fields = ('title_uk',)
    ordering = ('-category',)

    def get_fields(self, request, obj=None):
        if(obj):
            return ('title_uk', 'ref_title_uk', 'reference', 'alias', 'category', 'priority', 'meta_keywords_uk', 'preview_uk', 'text_uk', 'preview_formatted_uk', 'text_formatted_uk')
        return ('title_uk', 'ref_title_uk', 'reference', 'alias', 'category', 'priority', 'meta_keywords_uk', 'preview_uk', 'text_uk')

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ('preview_formatted_uk', 'text_formatted_uk')
        return readonly_fields

    def text_formatted_uk(self, obj):
        value = obj.text_formatted('uk')
        value = value.replace('\r\n', '')
        value = value.replace('\n', '')
        return mark_safe(value)

    def preview_formatted_uk(self, obj):
        value = obj.preview_formatted('uk')
        value = value.replace('\r\n', '')
        value = value.replace('\n', '')
        return mark_safe(value)

admin.site.register(Text, TextAdmin)