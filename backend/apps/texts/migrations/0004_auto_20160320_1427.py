# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0003_auto_20160320_1346'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='textimage',
            options={'verbose_name_plural': 'Text Images', 'verbose_name': 'Text Image', 'ordering': ['-created']},
        ),
        migrations.RenameField(
            model_name='text',
            old_name='meta_description',
            new_name='meta_description_de',
        ),
        migrations.RenameField(
            model_name='text',
            old_name='meta_keywords',
            new_name='meta_keywords_de',
        ),
        migrations.RenameField(
            model_name='text',
            old_name='preview',
            new_name='preview_de',
        ),
        migrations.RenameField(
            model_name='text',
            old_name='ref_title',
            new_name='ref_title_de',
        ),
        migrations.RenameField(
            model_name='text',
            old_name='text',
            new_name='text_de',
        ),
        migrations.RenameField(
            model_name='text',
            old_name='title',
            new_name='title_de',
        ),
        migrations.RenameField(
            model_name='textimage',
            old_name='description',
            new_name='description_de',
        ),
        migrations.RenameField(
            model_name='textimage',
            old_name='title',
            new_name='description_uk',
        ),
        migrations.RemoveField(
            model_name='textimage',
            name='name',
        ),
        migrations.AddField(
            model_name='text',
            name='meta_description_uk',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='text',
            name='meta_keywords_uk',
            field=models.CharField(blank=True, null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='text',
            name='preview_uk',
            field=tinymce.models.HTMLField(blank=True, verbose_name='Preview', null=True),
        ),
        migrations.AddField(
            model_name='text',
            name='ref_title_uk',
            field=models.CharField(null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='text',
            name='text_uk',
            field=tinymce.models.HTMLField(blank=True, verbose_name='Text', null=True),
        ),
        migrations.AddField(
            model_name='text',
            name='title_uk',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='textimage',
            name='title_de',
            field=models.CharField(blank=True, null=True, max_length=255),
        ),
        migrations.AddField(
            model_name='textimage',
            name='title_uk',
            field=models.CharField(blank=True, null=True, max_length=255),
        ),
    ]
