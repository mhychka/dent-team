# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0004_auto_20160320_1427'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='preview_de',
            field=tinymce.models.HTMLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='text',
            name='preview_uk',
            field=tinymce.models.HTMLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='text',
            name='text_de',
            field=tinymce.models.HTMLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='text',
            name='text_uk',
            field=tinymce.models.HTMLField(blank=True, null=True),
        ),
    ]
