# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0014_text_priority'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='category',
            field=models.CharField(default='', max_length=255),
        ),
    ]
