# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0010_auto_20160531_0936'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='meta_description_uk',
            field=models.TextField(blank=True, null=True),
        ),
    ]
