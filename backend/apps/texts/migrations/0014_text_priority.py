# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0013_auto_20160601_0405'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='priority',
            field=models.SmallIntegerField(max_length=3, default=0),
        ),
    ]
