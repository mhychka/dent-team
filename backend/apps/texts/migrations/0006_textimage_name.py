# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0005_auto_20160320_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='textimage',
            name='name',
            field=models.CharField(blank=True, null=True, max_length=255),
        ),
    ]
