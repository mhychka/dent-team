# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0008_text_reference'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='textimage',
            options={'verbose_name': 'Text Image', 'verbose_name_plural': 'Text Images', 'ordering': ['-priority', '-created']},
        ),
        migrations.AlterField(
            model_name='textimage',
            name='status',
            field=models.CharField(null=True, max_length=10),
        ),
    ]
