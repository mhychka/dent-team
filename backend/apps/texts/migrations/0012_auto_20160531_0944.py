# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0011_text_meta_description_uk'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='ref_title_uk',
            field=models.CharField(null=True, blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='text',
            name='reference',
            field=models.CharField(null=True, blank=True, max_length=255),
        ),
    ]
