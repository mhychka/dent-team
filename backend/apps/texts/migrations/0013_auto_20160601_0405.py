# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0012_auto_20160531_0944'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='meta_keywords_uk',
            field=models.TextField(blank=True, null=True),
        ),
    ]
