# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0006_textimage_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='textimage',
            name='link',
            field=models.CharField(max_length=255, blank=True, null=True),
        ),
    ]
