# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import texts.utils
from django.conf import settings
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', tinymce.models.HTMLField(verbose_name='Text')),
                ('preview', tinymce.models.HTMLField(verbose_name='Preview')),
                ('alias', models.CharField(max_length=255, unique=True, default='')),
                ('title', models.CharField(max_length=255)),
                ('ref_title', models.CharField(max_length=255, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('active', models.BooleanField(default=True)),
                ('meta_keywords', models.CharField(max_length=255, null=True)),
                ('meta_description', models.TextField(null=True)),
                ('owner', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Text',
                'verbose_name_plural': 'Texts',
            },
        ),
        migrations.CreateModel(
            name='TextImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', sorl.thumbnail.fields.ImageField(max_length=255, upload_to=texts.utils.generate_text_image_path)),
                ('description', models.CharField(blank=True, max_length=255, null=True)),
                ('name', models.CharField(max_length=255, null=True)),
                ('source_filename', models.CharField(max_length=255, null=True)),
                ('filename', models.CharField(max_length=255, null=True)),
                ('mime_type', models.CharField(max_length=30, null=True)),
                ('guid', models.CharField(max_length=10, unique=True, null=True)),
                ('jsguid', models.CharField(max_length=12, default='')),
                ('source', models.CharField(max_length=20, null=True, default='local')),
                ('size', models.PositiveIntegerField(null=True)),
                ('width', models.PositiveIntegerField(null=True)),
                ('height', models.PositiveIntegerField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('priority', models.SmallIntegerField(max_length=3, default=0)),
                ('status', models.CharField(max_length=4, null=True)),
                ('owner', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL)),
                ('text', models.ForeignKey(to='texts.Text', related_name='images')),
            ],
            options={
                'verbose_name': 'Text Image',
                'verbose_name_plural': 'Text Images',
            },
        ),
    ]
