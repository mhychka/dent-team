# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0009_auto_20160425_2110'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='text',
            name='meta_description_de',
        ),
        migrations.RemoveField(
            model_name='text',
            name='meta_description_uk',
        ),
        migrations.RemoveField(
            model_name='text',
            name='meta_keywords_de',
        ),
        migrations.RemoveField(
            model_name='text',
            name='preview_de',
        ),
        migrations.RemoveField(
            model_name='text',
            name='ref_title_de',
        ),
        migrations.RemoveField(
            model_name='text',
            name='text_de',
        ),
        migrations.RemoveField(
            model_name='text',
            name='title_de',
        ),
        migrations.RemoveField(
            model_name='textimage',
            name='description_de',
        ),
        migrations.RemoveField(
            model_name='textimage',
            name='title_de',
        ),
    ]
