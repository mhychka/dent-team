# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0002_remove_textimage_jsguid'),
    ]

    operations = [
        migrations.AddField(
            model_name='textimage',
            name='title',
            field=models.CharField(null=True, blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='text',
            name='meta_description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='text',
            name='meta_keywords',
            field=models.CharField(null=True, blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='text',
            name='preview',
            field=tinymce.models.HTMLField(null=True, blank=True, verbose_name='Preview'),
        ),
        migrations.AlterField(
            model_name='text',
            name='text',
            field=tinymce.models.HTMLField(null=True, blank=True, verbose_name='Text'),
        ),
    ]
