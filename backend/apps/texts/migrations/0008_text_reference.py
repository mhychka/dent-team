# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texts', '0007_textimage_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='reference',
            field=models.CharField(null=True, max_length=255),
        ),
    ]
