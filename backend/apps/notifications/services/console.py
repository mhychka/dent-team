from django.utils import timezone
import datetime
import pytz
from django.conf import settings


def console_notification(name, text, params=None, force=False):
    if not force and not settings.NOTIFICATIONS_CONSOLE_ENABLED:
        return

    print(str(datetime.datetime.now(timezone.utc)) + ' ' + name + ': ' + text)
    if params:
        print(params)
