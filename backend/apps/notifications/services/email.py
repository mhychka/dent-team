from django.conf import settings
from django.core.mail import send_mail

def email_debug_message(params, force=False):
    if not force and not settings.NOTIFICATIONS_EMAILDEBUG_ENABLED:
        return

    params['subject'] = settings.HOST_NAME+' debug: '+params.get('subject', '')
    params['from_email'] = settings.DEFAULT_FROM_EMAIL
    params['recipient_list'] = [email for name, email in settings.ADMINS]
    send_mail(**params)