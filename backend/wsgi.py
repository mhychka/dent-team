import os
from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings.local'
os.environ["CELERY_LOADER"] = "django"
application = get_wsgi_application()