from django.middleware.locale import LocaleMiddleware
from django.utils import translation
from authentication.models import AuthUser


class i18nMiddleware(LocaleMiddleware):

    def get_language_for_user(self, request):
        if hasattr(request, 'user') and request.user.is_authenticated():
            try:
                if(request.user.language):
                    return request.user.language
            except AuthUser.DoesNotExist:
                pass
        check_path = self.is_language_prefix_patterns_used()
        language = translation.get_language_from_request(request, check_path=check_path)
        return language

    def process_request(self, request):
        translation.activate(self.get_language_for_user(request))
        request.LANGUAGE_CODE = translation.get_language()