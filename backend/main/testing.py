from unittest.mock import patch

from django.test import TransactionTestCase
from django.test.runner import DiscoverRunner as BaseRunner


class NoDatabaseMixin(object):
    """
    Test runner mixin which skips the DB setup/teardown
    when there are no subclasses of TransactionTestCase to improve the speed
    of running the tests.
    """

    def build_suite(self, *args, **kwargs):
        """
        Check if any of the tests to run subclasses TransactionTestCase.
        """
        suite = super(NoDatabaseMixin, self).build_suite(*args, **kwargs)
        self._needs_db = any(
            [isinstance(test, TransactionTestCase) for test in suite])
        return suite

    def setup_databases(self, *args, **kwargs):
        """
        Skip test creation if not needed. Ensure that touching the DB raises and
        error.
        """
        if self._needs_db:
            return super(NoDatabaseMixin, self).setup_databases(*args,
                                                                **kwargs)
        if self.verbosity >= 1:
            print('No DB tests detected. Skipping Test DB creation...')
        self._db_patch = patch('django.db.backends.util.CursorWrapper')
        self._db_mock = self._db_patch.start()
        self._db_mock.side_effect = RuntimeError('No testing the database!')
        return None

    def teardown_databases(self, *args, **kwargs):
        """
        Remove cursor patch.
        """
        if self._needs_db:
            return super(NoDatabaseMixin, self).teardown_databases(*args,
                                                                   **kwargs)
        self._db_patch.stop()
        return None


class NoDbTestRunner(NoDatabaseMixin, BaseRunner):
    """Actual test runner sub-class to make use of the mixin."""
    pass


class TestCaseMixin():
    user_simple_fields = dict(first_name='test', last_name='test',
                       email='test@domain.com', username='test',
                       password='password')
    user_staff_fields = dict(first_name='staff', last_name='staff',
                       email='staff@domain.com', username='staff',
                       password='password', is_staff=True)
    user_super_fields = dict(first_name='super', last_name='super',
                       email='super@domain.com', username='super',
                       password='password')

    @staticmethod
    def format_fields(fields, args):
        return {k: (v.format(args)) for k, v in fields.items()}
