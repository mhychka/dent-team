from django import template
from utils.compilers import webpack_js as _webpack_js
from utils.compilers import webpack_js_i18n as _webpack_js_i18n
from utils.compilers import pipeline_css as _pipeline_css
register = template.Library()

@register.simple_tag
def webpack_js(package_name, request, cache=True):
    return _webpack_js(package_name, request, cache)

@register.simple_tag
def webpack_js_i18n(request, cache=True):
    return _webpack_js_i18n(request, cache)

@register.simple_tag
def pipeline_css(package_name, request, cache=True):
    return _pipeline_css(package_name, request, cache)