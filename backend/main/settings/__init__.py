import os
import site


def path(dir, path=None):
    if not path:
        path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    return os.path.join(path, dir).replace('\\', '/')

def packages_path(dir):
    for _path in site.getsitepackages():
        path = os.path.dirname(_path)
        site_path = os.path.join(path, 'site-packages').replace('\\', '/')
        site_path = os.path.join(site_path, dir).replace('\\', '/')
        dist_path = os.path.join(path, 'dist-packages').replace('\\', '/')
        dist_path = os.path.join(dist_path, dir).replace('\\', '/')

        if os.path.exists(site_path):
            return site_path

        if os.path.exists(dist_path):
            return dist_path


    return None

with open(path('../build.txt'), "r") as f:
    build_version = f.readline().strip()
    f.close()