import os
import sys
import djcelery
from kombu import Exchange, Queue
from main.settings import path, build_version, packages_path
from main.settings.css import packages_css
from django.utils.translation import ugettext_lazy as _
djcelery.setup_loader()

APPS_PATH = path('apps')
VENDORS_PATH = path('vendors')
MAIN_PATH = path('main')

sys.path.insert(0, APPS_PATH)
sys.path.insert(0, VENDORS_PATH)

# EMAILS
EMAIL_HOST_USER = 'info@dentteam.pp.ua'
SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 465

EMAIL_HOST_PASSWORD = ''
SEND_BROKEN_LINK_EMAILS = True

# KEYS
SECRET_KEY = 'vm4rl5*ymb@2&d_(gc$gb-^twq9w(u69hi--%$5xrh!xk(t%hw'

# DEBUG
DEBUG = False

# COMMON
APP_ENV = 'prod'
ADMINS = (('Max Gichka', 'mgi4ka@gmail.com'),)
MANAGERS = None
DEVELOPERS = (('Max Gichka', 'mgi4ka.dev@gmail.com'),)
ALLOWED_HOSTS = ['*']
HOST_NAME='127.0.0.1'
DEFAULT_HTTP_PROTOCOL = 'http'
HOST_BASE_URL = 'https://'+HOST_NAME
ROOT_URLCONF = 'main.urls'
WSGI_APPLICATION = 'wsgi.application'
SITE_ID = 1
SERVER_ID = 0
BUILD_VERSION = build_version
SITE_NAME = 'Dent Team'
TIME_ZONE = 'UTC'
DATE_FORMAT = "F d, Y"
USE_TZ = True
FILE_UPLOAD_MAX_SIZE = 45

INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.gis',
    'main',
    'sorl.thumbnail',
)
MIDDLEWARE_CLASSES = (
    #'django.middleware.cache.FetchFromCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'main.middlewares.i18nMiddleware',
    'minidetector.Middleware',
    'visitor.middleware.VisitorInformationMiddleware'
    #'djstripe.middleware.SubscriptionPaymentMiddleware',
)

#DB
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'dentteam_db',
        'USER': 'dentteam_user',
        'PASSWORD': 'dentteam123',
        'HOST': 'localhost'
    }
}

#TEMPLATES
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.jinja2.Jinja2',
        'DIRS': [
            path('../frontend/templates/jinja'),
        ],
        'OPTIONS': {
            'environment': 'main.settings.jinja2.environment',
            'autoescape': True,
            'auto_reload': True,
        }
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            path('../frontend/templates/django'),
            packages_path('adminplus/templates/adminplus'),
            packages_path('django/contrib/admin/templates/admin'),
            packages_path('ordered_model/templates/ordered_model'),
            packages_path('static_sitemaps/templates/static_sitemaps'),
            packages_path('django/contrib/sitemaps/templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.debug',
                'django.core.context_processors.i18n',
                'django.core.context_processors.request',
                'django.core.context_processors.static',
                'django.core.context_processors.media',
                'django.contrib.messages.context_processors.messages'
            ],
        },
    },
]

#STATICFILES
STATIC_ROOT = path('../frontend/static')
STATIC_URL = '/static/'
MEDIA_ROOT = path('../frontend/static/media')
MEDIA_URL = '/media/'
STATICFILES_DIRS = (
    #('bootstrap-social', path('../frontend/node_modules/bootstrap-social')),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
#STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

#LOGGING
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        # Include the default Django email handler for errors
        # This is what you'd get without configuring logging at all.
        'mail_admins': {
            'class': 'django.utils.log.AdminEmailHandler',
            'level': 'ERROR',
            # But the emails are plain text by default - HTML is nicer
            'include_html': True,
        }
    },
    'loggers': {
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        }
    },
}

#AUTHENTICATION
INSTALLED_APPS += ('django.contrib.auth', 'authentication',)
LOGIN_URL = '/user/login/'
LOGIN_REDIRECT_URL = '/'
AUTH_USER_MODEL = 'authentication.AuthUser'
AUTH_USER_REGISTER = False
AUTHENTICATION_BACKENDS = (
    'authentication.backends.EmailOrUsernameModelBackend',
    'django.contrib.auth.backends.ModelBackend'
)

#TINYMCE
INSTALLED_APPS += ('tinymce', 'mce_filebrowser')
TINYMCE_JS_URL = "/static/tiny_mce/tiny_mce.js"
TINYMCE_JS_ROOT = os.path.join(STATIC_ROOT, "tiny_mce")
TINYMCE_PLUGINS = [
    'safari',
    'table',
    #'advlink',
    #'advimage',
    #'iespell',
    'inlinepopups',
    'media',
    'searchreplace',
    'contextmenu',
    'paste',
    #'jbimages',
    #'image'
]
TINYMCE_DEFAULT_CONFIG = {
    'theme': "advanced",
    'mode': "textareas",
    'width': "674",
    'height': "600",
    'plugins': ",".join(TINYMCE_PLUGINS),  # django-cms
    'language': 'ru',
    'theme_advanced_buttons1': "formatselect,|,bold,italic,underline,strikethrough,|,"
                               "justifyleft,justifycenter,justifyright,justifyfull,|,"
                               "bullist,numlist,|,link,unlink,anchor,image",
    'theme_advanced_buttons2': "cut,copy,paste,pastetext,pasteword,|,"
                               "search,replace,|,undo,redo,|,link,unlink,cleanup,|,"
                               "table,|,delete_row,delete_table,|,row_after,row_before,"
                               "|,code",
    'theme_advanced_buttons3': "",
    'theme_advanced_buttons4': "",
    'theme_advanced_toolbar_location': "top",
    'theme_advanced_toolbar_align': "left",
    'theme_advanced_statusbar_location': "bottom",
    'theme_advanced_resizing': True,
    'table_default_cellpadding': 2,
    'table_default_cellspacing': 2,
    'cleanup_on_startup': False,
    'cleanup': False,
    'paste_auto_cleanup_on_paste': True,
    'paste_block_drop': False,
    'paste_remove_spans': False,
    'paste_strip_class_attributes': False,
    'paste_retain_style_properties': "",
    'remove_linebreaks': False,
    'convert_newlines_to_brs': False,
    'convert_urls' : False,
    'inline_styles': False,
    'relative_urls': False,
    'force_br_newlines' : False,
    'force_p_newlines' : True,
    'forced_root_block' : '',
    'formats': {
        'alignleft': {
            'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
            'classes': 'align-left'},
        'aligncenter': {
            'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
            'classes': 'align-center'},
        'alignright': {
            'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
            'classes': 'align-right'},
        'alignfull': {
            'selector': 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
            'classes': 'align-justify'},
        'strikethrough': {'inline': 'del'},
        'italic': {'inline': 'em'},
        'bold': {'inline': 'strong'},
        'underline': {'inline': 'u'}
    },
    'pagebreak_separator': "",
    'file_browser_callback': 'mce_filebrowser',
    'valid_elements': '*[*]'
}

#ADMINISTRATION
INSTALLED_APPS += (
    'django_admin_bootstrapped',
    'django.contrib.admin.apps.SimpleAdminConfig',
    'administration',
    'adminplus'
)

#i18n
INSTALLED_APPS += ('statici18n',)
LOCALE_PATHS = (path('../locale'),)
USE_L10N = True
USE_I18N = True
LANGUAGE_CODE = 'uk'
LANGUAGES = (
    ('uk', 'Ukrainian'),
)

#REDIS
REDIS_DB = 4
REDIS_HOST = 'localhost'
REDIS_PORT = 6379
REDIS_PASSWORD = None

#CELERY
INSTALLED_APPS += ('djcelery',)
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERY_TIMEZONE = 'Europe/Kiev'
CELERY_ALWAYS_EAGER = False
CELERY_SEND_TASK_ERROR_EMAILS = True
CELERY_IGNORE_RESULT = True
CELERY_DISABLE_RATE_LIMITS = True
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_QUEUES = (
    Queue('default', Exchange('default', type='direct'), routing_key='default'),
)
CELERY_DEFAULT_QUEUE = 'default'
CELERY_DEFAULT_EXCHANGE = 'default'
CELERY_DEFAULT_ROUTING_KEY = 'default'
CELERY_RESULT_BACKEND = 'redis'
CELERY_TASK_RESULT_EXPIRES = 18000
CELERY_REDIS_HOST = REDIS_HOST
CELERY_REDIS_PORT = REDIS_PORT
CELERY_REDIS_DB = REDIS_DB
BROKER_URL = "redis://{host}:{port}/{db}".format(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

#REST_FRAMEWORK
INSTALLED_APPS += ('rest_framework',)
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
    'DATE_FORMAT': "%m/%d/%Y",
    'DATE_INPUT_FORMATS': ("%d/%m/%Y", "%Y-%m-%d", "")
}

#COMPRESSOR
PIPELINE_ENABLED = False
INSTALLED_APPS += ('pipeline',)
STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'
STATICFILES_FINDERS += ('pipeline.finders.PipelineFinder',)
PIPELINE_CSS = packages_css
PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yuglify.YuglifyCompressor'
PIPELINE_YUGLIFY_BINARY = path('../frontend/node_modules/yuglify/bin/yuglify')

#VISITOR
VISITOR_DEFAULT_IP = None

#TEXTS
INSTALLED_APPS += ('texts',)

#ARTICLES
INSTALLED_APPS += ('articles',)

#CONTACTS
INSTALLED_APPS += ('contacts',)

#SITEMAP
INSTALLED_APPS += ('static_sitemaps',)
STATICSITEMAPS_ROOT_SITEMAP = 'main.sitemaps.sitemaps'
STATICSITEMAPS_USE_GZIP = False

#NOTIFICATIONS
NOTIFICATIONS_EMAILDEBUG_ENABLED = True

#BACKUP
BACKUP_DATABASE = True
BACKUP_DIR = path('../backup/dent-team/db')