from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.urlresolvers import reverse
from django.conf import settings
from jinja2 import Environment
from jinja2.ext import i18n
from pipeline.templatetags.ext import PipelineExtension
from utils.string import guid, strval
import json
from main.templatefilters import datetimeformat, active_class
from utils.compilers import pipeline_css, webpack_js
from django.utils import translation


def environment(**options):
    options['extensions'] = [PipelineExtension, i18n]
    env = Environment(**options)
    env.install_gettext_translations(translation)
    env.filters['jsonify'] = json.dumps
    env.filters['datetimeformat'] = datetimeformat

    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
        'guid': guid,
        'json':json,
        'len': len,
        'settings':settings,
        'str':str,
        'strval': strval,
        'active_class': active_class,
        'pipeline_css': pipeline_css,
        "webpack_js": webpack_js,
        'print': print,
        'lang': translation.get_language,
        'int': int
    })
    return env