from main.settings.base import *

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']
APP_ENV = 'dev'

#BACKUP
BACKUP_DATABASE = False

PIPELINE_ENABLED = False
DEBUG = True

VISITOR_DEFAULT_IP = "212.22.223.44" #Kiev
#VISITOR_DEFAULT_IP = "129.178.88.82" #Stockholm