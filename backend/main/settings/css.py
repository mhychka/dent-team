from main.settings import build_version

packages_css = {
    'app': {
        'source_filenames': (
            'bootstrap-social/bootstrap-social.css',
            'bootstrap/css/bootstrap.css',
            'bootstrap/css/font-awesome.css',
            'js/jquery/plugins/fancybox/helpers/jquery.fancybox-buttons.css',
            'js/jquery/plugins/fancybox/jquery.fancybox.css',
            'js/jquery/plugins/flexslider/flexslider.css',
            'css/overwrite.css',
            'css/animate.css',
            'css/fonts.css',
            'css/style.css',
            'css/default.css',
            'bootstrap-formhelpers/bootstrap-formhelpers.min.css'
        ),
        'output_filename': 'css/compressed/main.compressed.{build_version}.bundle.css'.format(build_version=build_version),
    },
    'fileupload': {
        'source_filenames': (
            'js/jquery/plugins/fileupload/jquery.fileupload.css',
        ),
        'output_filename': 'css/compressed/project_upload.compressed.{build_version}.bundle.css'.format(build_version=build_version),
    },
}