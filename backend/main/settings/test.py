from main.settings.base import *

APP_ENV = 'test'
TEST_RUNNER = 'main.testing.NoDbTestRunner'