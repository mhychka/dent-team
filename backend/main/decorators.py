from django.http import HttpResponse
from django.conf import settings

try:
    import json
except ImportError:
    import simplejson as json


def render_to(template, templatesDir=None):
    from django.shortcuts import render_to_response
    from django.template import RequestContext

    if templatesDir:
        template = templatesDir + '/' + template

    def renderer(func):
        def wrapper(request, *args, **kw):
            output = func(request, *args, **kw)
            if isinstance(output, (list, tuple)):
                return render_to_response(output[1], output[0],
                                          RequestContext(request))
            elif isinstance(output, dict):
                return render_to_response(template, output,
                                          RequestContext(request))
            return output

        return wrapper

    return renderer


def to_json_response(obj, **kwargs):
    if isinstance(obj, HttpResponse):
        return obj

    cls = kwargs.pop('cls', HttpResponse)
    jsonify = kwargs.pop('jsonify', True)
    try:
        params = {'mimetype': 'application/json'}
        params.update(kwargs)
        r = cls(**params)
    except TypeError:
        return obj

    if obj is not None:
        indent = 4 if getattr(settings, 'DEBUG', False) else None
        if jsonify:
            r.write(json.dumps(obj, indent=indent))
        else:
            r.write(obj)
    else:
        r.write("{}")
    return r


def render_to_json(default_args={}):
    def wrap(the_func):
        def _decorated(*args, **kwargs):
            ret = the_func(*args, **kwargs)

            obj = ret
            args = default_args.copy()

            if isinstance(ret, tuple):
                if len(ret) == 2:
                    obj, newargs = ret
                    if isinstance(newargs, dict):
                        args.update(newargs)

            return to_json_response(obj, **(args))

        _decorated.__name__ = the_func.__name__
        return _decorated

    return wrap