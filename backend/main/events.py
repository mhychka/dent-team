# -*- coding: utf-8 -*-
from utils.redis import get_redis_client
import json

def create_event(name, data):
    redis_client = get_redis_client()
    redis_client.publish(name, json.dumps(data))