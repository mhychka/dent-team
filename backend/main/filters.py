import jinja2


def priceformat(fmt_str, *args, **kwargs):
    if args and kwargs:
        raise jinja2.exceptions.FilterArgumentError(
            "can't handle positional and keyword "
            "arguments at the same time"
        )
    ufmt_str = jinja2.utils.soft_unicode(fmt_str)
    if kwargs:
        return ufmt_str.format(**kwargs)
    return ufmt_str.format(*args)