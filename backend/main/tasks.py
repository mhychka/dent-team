from celery.task import PeriodicTask
from celery import Task
from datetime import timedelta
import os
import datetime
from django.utils import timezone
from django.conf import settings
from utils.filesystem import makedir
from notifications.services.email import email_debug_message
from contacts.models import ContactMessage
from utils import email


class DBBackupTask(PeriodicTask):
    run_every = timedelta(days=1)
    queue = 'default'
    routing_key = 'default'
    target_path = None

    def run(self, *args, **kwargs):
        if not settings.BACKUP_DATABASE:
            return

        db = settings.DATABASES['default']['NAME']
        user = settings.DATABASES['default']['USER']
        password = settings.DATABASES['default']['PASSWORD']
        host = settings.DATABASES['default']['HOST']
        time = datetime.datetime.utcnow()
        target_filename = time.strftime(db + '_%Y-%m-%d_%H_%M.sql')
        target_dir = time.strftime(settings.BACKUP_DIR)

        makedir(target_dir)
        self.target_path = time.strftime("{target_dir}/{target_filename}").format(target_filename=target_filename, target_dir=target_dir)
        cmd = 'PGPASSWORD="{password}" pg_dump -U {user} -h {host} {db} > {target_path}'.format(
            db=db, target_path=self.target_path, host=host, user=user, password=password)
        os.system(cmd)

    def on_success(self, retval, task_id, args, kwargs):
        if not settings.BACKUP_DATABASE:
            return

        if not os.path.exists(self.target_path):
            return self.on_failure(retval, task_id, args, kwargs, None)

        email_debug_message({
            'subject': 'daily database dump created by server ' + str(settings.SERVER_ID),
            'message':  "date: {date}\npath: {path}\nserver_id: {server_id}".format(
                date=str(datetime.datetime.now(timezone.utc)),
                path=self.target_path,
                server_id=str(settings.SERVER_ID))
        })

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        if not settings.BACKUP_DATABASE:
            return

        email_debug_message({
            'subject': 'daily database dump failed',
            'message':  "date: {date}\npath: {path}\nserver_id: {server_id}".format(
                date=str(datetime.datetime.now(timezone.utc)),
                path=self.target_path,
                server_id=str(settings.SERVER_ID))
        })


class ContactMessageNotificationTask(Task):
    queue = 'default'
    routing_key = 'default'

    def run(self, pk, **kwargs):

        self.obj = ContactMessage.objects.get(pk=pk)
        self.obj.handling_attempt += 1
        self.obj.save()

        email.send_html(
            self.obj.subject,
            [settings.DEFAULT_FROM_EMAIL],
            {'obj':self.obj},
            template='contacts/email/contactmessage.html',
            reply_to=[self.obj.email]
        )

    def on_success(self, retval, task_id, args, kwargs):
        if self.obj:
            self.obj.status = ContactMessage.STATUS_HANDLED
            self.obj.save()


class ContactMessageHandleTask(PeriodicTask):
    run_every = timedelta(days=1)
    queue = 'default'
    routing_key = 'default'
    target_path = None

    def run(self, *args, **kwargs):
        objects = ContactMessage.objects.filter(status=ContactMessage.STATUS_PENDING)
        for obj in objects:
            if obj.handling_attempt > 2:
                obj.status = ContactMessage.STATUS_ERROR
                obj.save()

                email_debug_message({
                    'subject': 'contact message handling failed',
                    'message':  "date: {date}\npath: {obj_pk}\n".format(
                        date=str(datetime.datetime.now(timezone.utc)),
                        obj_pk=obj.pk)
                })
            else:
                task = ContactMessageNotificationTask()
                task.apply_async((obj.pk,))

