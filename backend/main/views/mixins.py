from django.http import JsonResponse
from django.http import HttpResponse
import json


class JSONResponseMixin(object):
    def render_to_response(self, context, **response_kwargs):
        return JsonResponse(self.get_data(context), **response_kwargs)

    def get_data(self, context):
        return context


class AjaxableCreateViewMixin(object):
    def render_to_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return self.render_to_response(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        object = super().form_valid(form)
        data = {'pk': self.object.pk, }
        return self.render_to_response(data)


class UserFormKwargsMixin(object):
    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs.update({"user": self.request.user})
        return kwargs