# -*- coding: utf-8 -*-
from django.views.generic.base import View, TemplateView
from texts.models import Text

class TestView(TemplateView):
    template_name = 'test.html'

    def get_context_data(self, **kwargs):
        from utils import email
        from contacts.models import ContactMessage
        

        objs = ContactMessage.objects.filter()[:1]
        obj = objs[0]

        email.send_html(
            'test subject',
            ['mgi4ka@gmail.com'],
            {'obj':obj},
            from_email='info@klinikumevb.com.ua',
            template='contacts/email/contactmessage.html',
        )


class AppView(TemplateView):
    template_name = 'app.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        try:
            teasers = Text.objects.prefetch_related('images').get(alias='index-teasers')
        except Text.DoesNotExist:
            teasers = None

        data['teasers'] = teasers
        return data