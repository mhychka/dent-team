
def datetimeformat(value, f='%B %d, %Y %H:%M'):
    return value.strftime(f)


def active_class(request, patterns, out='active'):
    import re
    if "default" in patterns.split() and request.path == '/':
        return "%s" % out

    return "%s" % out if len([p for p in patterns.split() if re.search(p, request.path)]) else ''