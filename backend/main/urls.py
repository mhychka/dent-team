from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.views.generic import TemplateView
from main.views.view import AppView, TestView
from adminplus.sites import AdminSitePlus
from django.views.generic.base import RedirectView
admin.site = AdminSitePlus()
admin.autodiscover()

urlpatterns = patterns('',
    # index
    url(r'^$', RedirectView.as_view(url='/app'), name='index'),

    #templates
    url(r'^test/$', TestView.as_view(), name='test'),
    url(r'^templates/home/$', TemplateView.as_view(template_name='templates/home.html'), name='template-home'),
    url(r'^templates/blog/$', TemplateView.as_view(template_name='templates/blog.html'), name='template-blog'),

    # authentication
    url(r'^user/', include('authentication.urls', app_name='authentication', namespace='authentication')),

    #api
    url(r'^api/v1/', include('api.urls', app_name='api', namespace='api')),

    # admin
    url(r'^admin/', include(admin.site.urls)),

    # articles
    url(r'^app/', AppView.as_view(), name='app'),

    #robots.txt
    (r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^sitemap.xml', include('static_sitemaps.urls')),
    )

if not settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += staticfiles_urlpatterns()
if settings.APP_ENV == 'dev':
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT }))

