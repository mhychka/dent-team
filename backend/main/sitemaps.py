import datetime
import os

from django.contrib import sitemaps
from django.core import urlresolvers
from django.conf import settings
from django.core.urlresolvers import reverse
from articles.models import Article


class ArticlesSitemap(sitemaps.GenericSitemap):
    changefreq = "weekly"
    priority = 0.5

    def location(self, obj):
        return '/app/articles/{alias}'.format(alias=obj.alias)

class PagesSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return [
            '/app/',
            '/app/text/about',
            '/app/text/services',
            '/app/text/prices',
            '/app/contacts'
        ]

    def location(self, item):
        return item

sitemaps = {
    'pages': PagesSitemap(),
    'articles': ArticlesSitemap({'date_field': 'updated', 'queryset': Article.objects.filter()}),
}